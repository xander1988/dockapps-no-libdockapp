#include "dockapp.h"
#include "weather_report.h"

char city[20] = "";
char upd_time[5] = "";
char temperature[5] = "n/a";
char wind[30] = "n/a";
char humidity[30] = "n/a";
char pressure[30] = "n/a";
char windchill[5] = "n/a";

int speed = 0;

int download_report(char *command, char *home_dir) {
    static time_t stamp = 0;

    chdir(home_dir);

    /* update every 30 min */
    if (((time(NULL) - stamp) > 1800) || (!stamp)) {
        stamp = time(NULL);

        execute_command(command);

        return 1;
    }

    return 0;
}

int parse_report(char *path) {
    FILE *file;

    char *line, *ii;

    size_t len = 0;

    ssize_t read;

    int i, linenum = 0;

    strcpy(temperature, "n/a");
    strcpy(wind, "n/a");
    strcpy(humidity, "n/a");
    strcpy(pressure, "n/a");
    strcpy(windchill, "n/a");

    speed = 0;

    if ((file = fopen(path, "r")) == NULL)
        return 0;

    while ((read = getline(&line, &len, file)) != -1) {
        linenum++;

        for (i = 0; ; i++) {
            if (line[i] == '\0') {
                break;
            }

            /* city */
            if (linenum == 1) {
                if (line[i] == ',') {
                    break;
                }

                city[i] = line[i];
            }

            /* time */
            if (linenum   == 2   &&
                line[i]   == 'U' &&
                line[i+1] == 'T' &&
                line[i+2] == 'C') {
                upd_time[0] = line[i-5];
                upd_time[1] = line[i-4];
                upd_time[2] = line[i-3];
                upd_time[3] = line[i-2];
            }

            /* temperature */
            if (!strncmp(line, "Temp", 4)) {
                char *str = strstr(line, " ");

                char *tok = strtok(str, " ");

                strcpy(temperature, tok);
            }

            /* wind */
            if (!strncmp(line, "Wind:", 5)) {
                if (strstr(line, " Calm")) {
                    strcpy(wind, "calm");

                    continue;
                }

                if (strstr(line, " Variable "))
                    memcpy(wind, "vrb", 3);
                else
                    for (int n = 0; n < 3; n++)
                        if (line[n+15] == ' ')
                            break;
                        else
                            wind[n] = line[n+15];

                ii = strstr(line, " MPH");

                ii--;

                while (*ii != ' ')
                    ii--;

                ii++;

                speed = strtol(ii, &ii, 10);
            }

            /* windchill */
            if (!strncmp(line, "Windchill", 9)) {
                char *str = strstr(line, " ");

                char *tok = strtok(str, " ");

                strcpy(windchill, tok);
            }

            /* humidity */
            if (!strncmp(line, "Relative", 8)) {
                char *str = strstr(line, "Humidity: ");

                char *tok = strtok(str, "Humidity: ");

                strcpy(humidity, tok);
            }

            /* pressure */
            if (!strncmp(line, "Pressure", 8)) {
                char *str = strstr(line, "(altimeter): ");

                char *tok = strtok(str, "(altimeter): ");

                strcpy(pressure, tok);
            }
        }
    }

    fclose(file);

    if (line)
        FREE(line);

    return 1;
}
