/*
 *    WMCPULoad - A dockapp to monitor CPU usage
 *    Copyright (C) 2001,2002  Seiichi SATO <ssato@sh.rim.or.jp>

 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.

 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "weather_report.h"
#include "dockapp.h"
#include "pixmaps/master.xpm"
#include "pixmaps/master_2.xpm"
#include "pixmaps/master_3.xpm"
#include "pixmaps/master_4.xpm"
#include "pixmaps/mask.xbm"
#include "pixmaps/mask_2.xbm"
#include "pixmaps/mask_3.xbm"
#include "pixmaps/mask_4.xbm"

typedef enum { LIGHTON, LIGHTOFF } light;

static int time_diff = 0;

static unsigned	update_interval = 1;

static light backlight = LIGHTOFF;

static char *station = NULL;
static char *home_dir = NULL;

static char path[256] = "";
static char url[70] = "";
static char command[256] = "";

static bool knots = false;
static bool kilometers = false;
static bool meters = false;
static bool beaufort = false;
static bool celsius = false;
static bool mmHg = false;
static bool show_wchill = false;

/* prototypes */
static void update(void);
static void redraw(void);
static void switch_light(void);
static void draw_temp(char *);
static void draw_station(void);
static void draw_time(void);
static void draw_wind(int);
static void draw_hum(void);
static void draw_press(void);
static void draw_text(char *, int, int);
static void parse_arguments(int, char **);
static void print_help(void);

int main(int argc, char **argv) {
    XEvent Event;

    /* Parse Command-Line */
    parse_arguments(argc, argv);

    if (!station) {
        printf("Error: ICAO location indicator not set, exiting.\n");

        print_help(), exit(1);
    }

    home_dir = getenv("HOME");

    sprintf(path, "%s/%s", home_dir, station);
    sprintf(url, URL, station);
    sprintf(command, "wget -q -t 3 -U \"weather app\" %s -O %s", url, station);

    /* Initialize Application */
    if (scale == 1)
        open_window(argc, argv, (char **)master_xpm, (char *)mask_bits, mask_width, mask_height);

    if (scale == 2)
        open_window(argc, argv, (char **)master_2_xpm, (char *)mask_2_bits, mask_2_width, mask_2_height);

    if (scale == 3)
        open_window(argc, argv, (char **)master_3_xpm, (char *)mask_3_bits, mask_3_width, mask_3_height);

    if (scale == 4)
        open_window(argc, argv, (char **)master_4_xpm, (char *)mask_4_bits, mask_4_width, mask_4_height);

    /* Initialize pixmap */
    if (backlight == LIGHTON) {
	    copy_xpm_area(128, 0, 64, 64, 0, 0);
    } else {
	    copy_xpm_area(64, 0, 64, 64, 0, 0);
    }

    redraw();

    /* Main loop */
    for (;;) {
        while (XPending(display)) {
			XNextEvent(display, &Event);

			switch (Event.type) {
			case Expose:
				redraw();

				break;

			case DestroyNotify:
				XCloseDisplay(display);

				exit(0);

				break;

		    case ClientMessage:
                if (Event.xclient.data.l[0] == (int)deleteWin) {
                    exit(0);
                }

                break;

            case ButtonPress:
		        switch_light();

		        redraw();

		        break;
		    }
		}

		redraw();

		update();

	    usleep(update_interval * 100000L);
    }

    return 0;
}

/* called by timer */
static void update(void) {
    static int counter = 30;

    download_report(command, home_dir);

    if (access(station, F_OK) == 0) {
        parse_report(path);

        if (strcmp(windchill, "n/a") && show_wchill) {
            if (counter >= 15 && counter <= 30)
                draw_temp(temperature);
            else if (counter >= 0 && counter <= 14)
                draw_temp(windchill);

        } else {
            draw_temp(temperature);
        }

        draw_station();

        draw_time();

        draw_wind(counter);

        if (strcmp(humidity, "n/a") && strcmp(pressure, "n/a")) {
            if (counter >= 15 && counter <= 30)
                draw_press();
            else if (counter >= 0 && counter <= 14)
                draw_hum();

        } else {
            draw_press();

            draw_hum();
        }
    }

    counter--;

    if (counter < 0)
        counter = 30;

    redraw_window();
}

/* called when mouse button pressed */
static void switch_light(void) {
    switch (backlight) {
	case LIGHTOFF:
	    backlight = LIGHTON;

	    break;

	case LIGHTON:
	    backlight = LIGHTOFF;

	    break;
    }
}

static void redraw(void) {
    copy_xpm_area(backlight == LIGHTON ? 128 : 64, 0, 64, 64, 0, 0);
}

static void draw_temp(char *temp) {
    int tmp = 0;
    int v10, v1;
    int y = 0;

    if (!strcmp(temp, "n/a"))
        return;

    tmp = atoi(temp);

    if (celsius)
        tmp = (tmp - 32) / 1.8;

    /* make it positive for now */
    if (tmp < 0) {
        tmp =- tmp;

        /* draw minus */
        if (tmp / 10 != 0) {
            if (backlight == LIGHTON)
                copy_xpm_area(22, 93, 6, 2, 10, 19);
            else
                copy_xpm_area(22, 73, 6, 2, 10, 19);
        } else {
            if (backlight == LIGHTON)
                copy_xpm_area(22, 93, 6, 2, 22, 19);
            else
                copy_xpm_area(22, 73, 6, 2, 22, 19);
        }
    }

    v10 = tmp / 10;
    v1 = tmp - v10 * 10;

    if (backlight == LIGHTON) {
	    y = 20;
    }

    /* draw digit */
    copy_xpm_area(v1 * 10, 64+y, 10, 20, 29+3, 7+3);

    if (v10 != 0) {
	    copy_xpm_area(v10 * 10, 64+y, 10, 20, 17+3, 7+3);
    }

    /* draw c or f */
    if (backlight == LIGHTON) {
        if (celsius)
            copy_xpm_area(100, 78, 14, 14, 44, 10);
        else
	        copy_xpm_area(114, 78, 14, 14, 44, 10);
    } else {
        if (celsius)
            copy_xpm_area(100, 64, 14, 14, 44, 10);
        else
            copy_xpm_area(114, 64, 14, 14, 44, 10);
    }
}

static void draw_station(void) {
    draw_text(station, 8, 35);
}

static void draw_time(void) {
    int tmp = 0;
    
    char time_text[5] = "";
    
    tmp = atoi(upd_time);
    tmp += (time_diff * 100);
    
    sprintf(time_text, "%d", tmp);
    
    draw_text(time_text, 34, 35);
}

static void draw_wind(int counter) {
    char tmp[40] = "";

    if (!strcmp(wind, "n/a"))
        return;

    if (knots) {
        double speed_kt = (double)speed;

        speed_kt *= 0.8688; /* kt = sm/h */

        speed = speed_kt;
    }

    if (meters) {
        double speed_m = (double)speed;

        speed_m *= 0.4469; /* m/s */

        speed = speed_m;
    }

    if (kilometers) {
        double speed_k = (double)speed;

        speed_k *= 1.6090; /* km/h */

        speed = speed_k;
    }

    if (beaufort) {
        double speed_b = (double)speed;

        speed_b *= 0.4469; /* m/s */

        if (speed_b <= 0.2)
            speed_b = 0;
        else if (speed_b >  0.2 && speed_b <=  1.5)
            speed_b = 1;
        else if (speed_b >  1.5 && speed_b <=  3.3)
            speed_b = 2;
        else if (speed_b >  3.3 && speed_b <=  5.4)
            speed_b = 3;
        else if (speed_b >  5.4 && speed_b <=  7.9)
            speed_b = 4;
        else if (speed_b >  7.9 && speed_b <= 10.7)
            speed_b = 5;
        else if (speed_b > 10.7 && speed_b <= 13.8)
            speed_b = 6;
        else if (speed_b > 13.8 && speed_b <= 17.1)
            speed_b = 7;
        else if (speed_b > 17.1 && speed_b <= 20.7)
            speed_b = 8;
        else if (speed_b > 20.7 && speed_b <= 24.4)
            speed_b = 9;
        else if (speed_b > 24.4 && speed_b <= 28.4)
            speed_b = 10;
        else if (speed_b > 28.4 && speed_b <= 32.6)
            speed_b = 11;
        else if (speed_b > 32.6 && speed_b <= 36.9)
            speed_b = 12;
        else {
            speed_b = 13;
        }

        speed = speed_b;
    }

    speed = (int)(speed + 0.5);

    if (counter >= 15 && counter <= 30)
        sprintf(tmp, "wnd %s", wind);
    else if (counter >= 0 && counter <= 14)
        sprintf(tmp, "wnd %d", speed);

    draw_text(tmp, 8, 43);
}

static void draw_hum(void) {
    char tmp[40] = "";

    if (!strcmp(humidity, "n/a"))
        return;

    sprintf(tmp, "hum %s", humidity);

    draw_text(tmp, 8, 51);
}

static void draw_press(void) {
    char tmp[50] = "";

    double prs = 0;

    int l = 0;

    if (!strcmp(pressure, "n/a"))
        return;

    prs = atof(pressure);

    if (mmHg) {
        prs *= 25.4;

        l = (int)(prs + 0.5);

        sprintf(tmp, "prs %d", l);

    } else {
        /*if (prs < 100)
            l = (int)(prs * 100 + 0.5);
        else
            l = (int)(prs * 100 + 50);*/

        sprintf(tmp, "prs %g", prs);
    }

    draw_text(tmp, 8, 51);
}

static void draw_text(char *text, int dx, int dy) {
    size_t i;

    int k, c;

    k = dx;

    for (i = 0; i < strlen(text); i++) {
        c = toupper(text[i]);

        if (c == '%') {
            if (backlight == LIGHTON)
                copy_xpm_area(130, 131, 5, 7, k, dy);
            else
                copy_xpm_area(130, 124, 5, 7, k, dy);

            k += 6;

        } else if (c == '.') {
            if (backlight == LIGHTON)
                copy_xpm_area(135, 131, 2, 7, k, dy);
            else
                copy_xpm_area(135, 124, 2, 7, k, dy);

            k += 2;

        /*} else if (c == '-') {
            da_copy_xpm_area(176, 29, 4, 8, k, dy);

            k += 4;

        } else if (c == '_') {
            da_copy_xpm_area(172, 29, 4, 8, k, dy);

            k += 4;*/

        } else if (c == ' ') {
            if (backlight == LIGHTON)
                copy_xpm_area(2, 86, 5, 7, k, dy);
            else
                copy_xpm_area(2, 66, 5, 7, k, dy);

            k += 6;

        } else if (c >= 'A' && c <= 'Z') {
            c -= 'A';

            if (backlight == LIGHTON)
                copy_xpm_area(0 + c * 5, 131, 5, 7, k, dy);
            else
                copy_xpm_area(0 + c * 5, 124, 5, 7, k, dy);

            k += 6;

        } else {
            c -= '0';

            if (backlight == LIGHTON)
                copy_xpm_area(50 + c * 5, 117, 5, 7, k, dy);
            else
                copy_xpm_area(0 + c * 5, 117, 5, 7, k, dy);

            k += 6;
        }
    }
}

static void parse_arguments(int argc, char **argv) {
    int i;

    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h")) {
            print_help(), exit(0);

        } else if (!strcmp(argv[i], "--version") || !strcmp(argv[i], "-v")) {
            printf("%s version %s\n", NAME, VERSION), exit(0);

        } else if (!strcmp(argv[i], "--station") || !strcmp(argv[i], "-S")) {
            station = argv[++i];

        } else if (!strcmp(argv[i], "--celsius") || !strcmp(argv[i], "-c")) {
            celsius = true;

        } else if (!strcmp(argv[i], "--knots") || !strcmp(argv[i], "-k")) {
            knots = true;

        } else if (!strcmp(argv[i], "--meters") || !strcmp(argv[i], "-m")) {
            meters = true;

        } else if (!strcmp(argv[i], "--kilometers") || !strcmp(argv[i], "-K")) {
            kilometers = true;

        } else if (!strcmp(argv[i], "--beaufort")) {
            beaufort = true;

        } else if (!strcmp(argv[i], "--mmhg")) {
            mmHg = true;

        } else if (!strcmp(argv[i], "--windchill") || !strcmp(argv[i], "-w")) {
            show_wchill = true;

        } else if (!strcmp(argv[i], "--time-difference") || !strcmp(argv[i], "-t")) {
            time_diff = atoi(argv[++i]);
        
        } else if (!strcmp(argv[i], "--backlight") || !strcmp(argv[i], "-b")) {
            backlight = LIGHTON;

        } else if (!strcmp(argv[i], "--backlight-color") || !strcmp(argv[i], "-B")) {
            backlight_color = argv[++i];

        } else if (!strcmp(argv[i], "--display")) {
            display_name = argv[++i];

        } else if (!strcmp(argv[i], "--dark-color")) {
            dark_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--light-color")) {
            light_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--geometry")) {
            geometry = argv[++i];

        } else if (!strcmp(argv[i], "--scale")) {
            char *tmp = argv[++i];

            if (tmp == NULL)
                scale = 1;
            else
                scale = atoi(tmp);

            if (scale < 1 || scale > 4) {
                printf("Warning: scale must be >= 1 and <= 4.\n");

                scale = 1;
            }

        } else if (!strcmp(argv[i], "--window")) {
            windowed = 1;

        } else if (!strcmp(argv[i], "--dock")) {
            docked = 1;

        } else if (!strcmp(argv[i], "--image")) {
            image = argv[++i];

        } else {
            fprintf(stderr, "%s: unrecognized option '%s'\n", argv[0], argv[i]);

            print_help(), exit(1);
        }
    }
}

static void print_help() {
    printf(
        "%s version: %s\n"
        "usage:\n"
        "  -S --station          STATION   Required: this option tells "NAME" which METAR station to show data for. It can be any four-letter ICAO location indicator, which can be found at https://en.wikipedia.org/wiki/ICAO_airport_code.\n"
        "  -c --celsius                    Use celsius degrees.\n"
        "  -k --knots                      Wind speed in knots.\n"
        "  -m --meters                     Wind speed in meters per second.\n"
        "  -K --kilometers                 Wind speed in kilometers per second.\n"
        "     --beaufort                   Wind speed in Beaufort.\n"
        "     --mmhg                       Pressure in units of millimeters of Mercury.\n"
        "  -w --windchill                  Display windchill, if avaliable.\n"
        "  -t --time-difference  HOURS     Set user time difference.\n"
        "  -b --backlight                  Turn on back-light.\n"
        "  -B --backlight-color  COLOR     Back-light color (rgb:6E/C6/3B is default).\n"
        "     --display          DISPLAY   Use alternate X display.\n"
        "     --dark-color       COLOR     Set border dark color.\n"
        "     --light-color      COLOR     Set border light color.\n"
        "     --geometry         GEOMETRY  Set window geometry.\n"
        "     --scale            SCALE     Set dockapp scale (from 1 to 4).\n"
        "     --window                     Run in windowed mode.\n"
        "     --dock                       In the windowed mode, run as a panel.\n"
        "     --image            IMAGE     Set background xpm image.\n"
        "  -h --help                       Print this help.\n",
        NAME, VERSION
    );
}
/* ex:set sw=4 softtabstop=4: */
