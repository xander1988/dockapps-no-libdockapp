#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define URL "https://tgftp.nws.noaa.gov/data/observations/metar/decoded/%s.TXT"

extern int speed;

extern char city[20];
extern char upd_time[5];
extern char temperature[5];
extern char wind[30];
extern char humidity[30];
extern char pressure[30];
extern char windchill[5];

int download_report(char *, char *);
int parse_report(char *);
