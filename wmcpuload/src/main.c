/*
 *    WMCPULoad - A dockapp to monitor CPU usage
 *    Copyright (C) 2001,2002  Seiichi SATO <ssato@sh.rim.or.jp>

 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.

 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.

 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "dockapp.h"
#include "cpu.h"
#include "pixmaps/master.xpm"
#include "pixmaps/master_2.xpm"
#include "pixmaps/master_3.xpm"
#include "pixmaps/master_4.xpm"
#include "pixmaps/mask.xbm"
#include "pixmaps/mask_2.xbm"
#include "pixmaps/mask_3.xbm"
#include "pixmaps/mask_4.xbm"

#define MAX_HISTORY 16
#define CPUNUM_NONE -1

typedef enum { LIGHTON, LIGHTOFF } light;

static unsigned	update_interval = 1;
static light	backlight = LIGHTOFF;
static unsigned	alarm_threshold = 101;
static cpu_options cpu_opts;
static int	history[MAX_HISTORY];	/* history of cpu usage */
static int	hindex = 0;

/* prototypes */
static void update(void);
static void redraw(void);
static void switch_light(void);
static void draw_digit(int per);
static void parse_arguments(int argc, char **argv);
static void print_help(void);
#if USE_SMP
static void draw_cpunumber(void);
#endif

int
main(int argc, char **argv)
{
    XEvent Event;

    /* Parse Command-Line */
    cpu_opts.ignore_nice = False;
    cpu_opts.cpu_number = CPUNUM_NONE;
    parse_arguments(argc, argv);

    /* Initialize Application */
    cpu_init();
    if (scale == 1)
        open_window(argc, argv, (char **)master_xpm, (char *)mask_bits, mask_width, mask_height);

    if (scale == 2)
        open_window(argc, argv, (char **)master_2_xpm, (char *)mask_2_bits, mask_2_width, mask_2_height);

    if (scale == 3)
        open_window(argc, argv, (char **)master_3_xpm, (char *)mask_3_bits, mask_3_width, mask_3_height);

    if (scale == 4)
        open_window(argc, argv, (char **)master_4_xpm, (char *)mask_4_bits, mask_4_width, mask_4_height);

    /* Initialize pixmap */
    if (backlight == LIGHTON) {
	    copy_xpm_area(128, 0, 64, 64, 0, 0);
    } else {
	    copy_xpm_area(64, 0, 64, 64, 0, 0);
    }
    redraw();

    /* Main loop */
    for (;;) {
        while (XPending(display)) {
			XNextEvent(display, &Event);
			switch (Event.type) {
			case Expose:
				redraw();
				break;
			case DestroyNotify:
				XCloseDisplay(display);
				exit(0);
				break;
		    case ClientMessage:
                if (Event.xclient.data.l[0] == (int)deleteWin) {
                    exit(0);
                }
                break;
            case ButtonPress:
		        switch_light();
		        redraw();
		        break;
		    }
		}
		update();
	    usleep(update_interval * 100000L);
    }

    return 0;
}

/* called by timer */
static void
update(void)
{
    int usage;
    int x, h;

    static light pre_backlight;
    static Bool in_alarm_mode = False;

    if (backlight == LIGHTON) {
	x = 2;
    } else {
	x = 0;
    }

    /* get current cpu usage in percent */
    usage = cpu_get_usage(&cpu_opts);
    hindex++;
    if (hindex >= MAX_HISTORY) {
	hindex = 0;
    }
    history[hindex] = usage;

    /* alarm mode */
    if (usage >= (int)alarm_threshold) {
	if (!in_alarm_mode) {
	    in_alarm_mode = True;
	    pre_backlight = backlight;
	}
	if (backlight == LIGHTOFF) {
	    switch_light();
	    redraw();
	    return;
	}
    } else {
	if (in_alarm_mode) {
	    in_alarm_mode = False;
	    if (backlight != pre_backlight) {
		switch_light();
		redraw();
		return;
	    }
	}
    }

    /* slide past chart */
    copy_xpm_area(12, 36, 44, 21, 9, 36);
    copy_xpm_area((backlight == LIGHTOFF ? 64 : 128)+54, 36, 2, 21, 54, 36);

    /* clear digit */
    copy_xpm_area((backlight == LIGHTOFF ? 64 : 128)+8, 10, 34, 20, 8, 10);

    /* draw digit */
    draw_digit(usage);

#ifdef USE_SMP
    /* draw cpu number */
    if (cpu_opts.cpu_number != CPUNUM_NONE)
	draw_cpunumber();
#endif

    /* draw current chart */
    h = (21 * usage) / 100;
    copy_xpm_area(100+x, 64+21-h, 2, h, 51+3, 54-h+3);

    redraw_window();
}

/* called when mouse button pressed */
static void
switch_light(void)
{
    switch (backlight) {
	case LIGHTOFF:
	    backlight = LIGHTON;
	    break;
	case LIGHTON:
	    backlight = LIGHTOFF;
	    break;
    }
}

static void
redraw(void)
{
    int h, i, j = hindex;
    int x = 0;

    if (backlight == LIGHTON) {
	x = 2;
    } else {
	x = 0;
    }

    copy_xpm_area(backlight == LIGHTON ? 128 : 64, 0, 64, 64, 0, 0);

    /* redraw digit */
    draw_digit(history[hindex]);

#ifdef USE_SMP
    /* draw cpu number */
    if (cpu_opts.cpu_number != CPUNUM_NONE)
	draw_cpunumber();
#endif

    /* redraw chart */
    for (i = 0; i < MAX_HISTORY; i++) {
	h = (21 * history[j]) / 100;
	copy_xpm_area(100+x, 64+21-h, 2, h, 3+51-3*i, 3+54-h);
	j--;
	if (j < 0) j = MAX_HISTORY - 1;
    }
}

static void
draw_digit(int per)
{
    int v100, v10, v1;
    int y = 0;

    if (per < 0) per = 0;
    if (per > 100) per = 100;

    v100 = per / 100;
    v10  = (per - v100 * 100) / 10;
    v1   = (per - v100 * 100 - v10 * 10);

    if (backlight == LIGHTON) {
	y = 20;
    }

    /* draw digit */
    copy_xpm_area(v1 * 10, 64+y, 10, 20, 29+3, 7+3);
    if (v10 != 0) {
	copy_xpm_area(v10 * 10, 64+y, 10, 20, 17+3, 7+3);
    }
    if (v100 == 1) {
	copy_xpm_area(10, 64+y, 10, 20,  5+3, 7+3);
	copy_xpm_area(0, 64+y, 10, 20, 17+3, 7+3);
	copy_xpm_area(0, 64+y, 10, 20, 29+3, 7+3);
    }

}


#ifdef USE_SMP
static void
draw_cpunumber(void)
{
    int y, v1 = 0, v2 = 0;

    v2 = cpu_opts.cpu_number / 10;
    v1 = cpu_opts.cpu_number - v2 * 10;

    y = backlight == LIGHTON ? 126 : 116;

    //if (v2)
    copy_xpm_area(v2 * 6, y, 7, 11, 45, 12);

    copy_xpm_area(v1 * 6, y, 7, 11, 51, 12);
}
#endif

static void
parse_arguments(int argc, char **argv)
{
    int i;
    for (i = 1; i < argc; i++) {
	if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h"))
	    print_help(), exit(0);

	else if (!strcmp(argv[i], "--version") || !strcmp(argv[i], "-v"))
	    printf("%s version %s\n", NAME, VERSION), exit(0);

	else if (!strcmp(argv[i], "--backlight") || !strcmp(argv[i], "-b"))
	    backlight = LIGHTON;

	else if (!strcmp(argv[i], "--backlight-color") || !strcmp(argv[i], "-B")) {
	    backlight_color = argv[i + 1];
	    i++;
	}
#ifdef IGNORE_NICE
	else if (!strcmp(argv[i], "--ignore-nice") || !strcmp(argv[i], "-i"))
	    cpu_opts.ignore_nice = True;
#endif

	else if (!strcmp(argv[i], "--interval") || !strcmp(argv[i], "-I")) {
	    int integer;
	    if (argc == i + 1)
		fprintf(stderr,
			"%s: error parsing argument for option %s\n",
			argv[0], argv[i]), exit(1);
	    if (sscanf(argv[i + 1], "%i", &integer) != 1)
		fprintf(stderr,
			"%s: error parsing argument for option %s\n",
			argv[0], argv[i]), exit(1);
	    if (integer < 1)
		fprintf(stderr, "%s: argument %s must be >=1\n",
			argv[0], argv[i]), exit(1);
	    update_interval = integer;
	    i++;
	} else if (!strcmp(argv[i], "--alarm") || !strcmp(argv[i], "-a")) {
	    int integer;
	    if (argc == i + 1)
		alarm_threshold = 90;
	    else if (sscanf(argv[i + 1], "%i", &integer) != 1)
		alarm_threshold = 90;
	    else if (integer < 0 || integer > 100)
		fprintf(stderr, "%s: argument %s must be from 0 to 100\n",
			argv[0], argv[i]), exit(1);
	    else
		alarm_threshold = integer, i++;
	}

#ifdef USE_SMP
	else if (!strcmp(argv[i], "--cpu") || !strcmp(argv[i], "-c")) {
	    int integer;
	    if (argc == i + 1)
		fprintf(stderr, "%s: error parsing argument for option %s\n",
			argv[0], argv[i]), exit(1);
	    if (sscanf(argv[i + 1], "%i", &integer) != 1)
		fprintf(stderr, "%s: error parsing argument for option %s\n",
			argv[0], argv[i]), exit(1);
	    if (integer < 0)
		fprintf(stderr, "%s: argument %s must be >=0\n",
			argv[0], argv[i]), exit(1);
	    cpu_opts.cpu_number = integer;
	    i++;
	}
#endif	/* USE_SMP */

	else if (!strcmp(argv[i], "--display")) {
        display_name = argv[++i];

    } else if (!strcmp(argv[i], "--dark-color")) {
        dark_bd_color = argv[++i];

    } else if (!strcmp(argv[i], "--light-color")) {
        light_bd_color = argv[++i];

    } else if (!strcmp(argv[i], "--geometry")) {
        geometry = argv[++i];

    } else if (!strcmp(argv[i], "--scale")) {
        char *tmp = argv[++i];

        if (tmp == NULL)
            scale = 1;
        else
            scale = atoi(tmp);

        if (scale < 1 || scale > 4) {
            printf("Warning: scale must be >= 1 and <= 4.\n");

            scale = 1;
        }

    } else if (!strcmp(argv[i], "--window")) {
        windowed = 1;

    } else if (!strcmp(argv[i], "--dock")) {
        docked = 1;

    } else if (!strcmp(argv[i], "--image")) {
        image = argv[++i];
    }

	else {
	    fprintf(stderr, "%s: unrecognized option '%s'\n", argv[0],
		    argv[i]);
	    print_help(), exit(1);
	}
    }
}

static void
print_help()
{
    printf(
        "%s version: %s\n"
        "usage:\n"
        "  -b --backlight                    Turn on back-light.\n"
        "  -B --backlight-color  COLOR       Back-light color (rgb:6E/C6/3B is default).\n"
        "  -I --interval         INTERVAL    Number of secs between updates (1 is default).\n"
#ifdef IGNORE_NICE
        "  -i --ignore-nice                  Ignore a nice value.\n"
#endif
#ifdef USE_SMP
        "  -c --cpu              NUMBER      Which CPU is monitored (0, 1, ...).\n"
#endif
        "  -a --alarm            PERCENTAGE  Activate alarm mode. PERCENTAGE is threshold of percentage from 0 to 100 (90 is default).\n"
        "     --display          DISPLAY     Use alternate X display.\n"
        "     --dark-color       COLOR       Set border dark color.\n"
        "     --light-color      COLOR       Set border light color.\n"
        "     --geometry         GEOMETRY    Set window geometry.\n"
        "     --scale            SCALE       Set dockapp scale (from 1 to 4).\n"
        "     --window                       Run in windowed mode.\n"
        "     --dock                         In the windowed mode, run as a panel.\n"
        "     --image            IMAGE       Set background xpm image.\n"
        "  -h --help                         Print this help.\n",
        NAME, VERSION
    );
}
/* ex:set sw=4 softtabstop=4: */
