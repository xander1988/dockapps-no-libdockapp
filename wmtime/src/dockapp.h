#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <stdarg.h>
#include <sys/wait.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/xpm.h>
#include <X11/extensions/shape.h>

#define NAME "wmtime"
#define VERSION "1.6"
#define MAIN_COLOR "#00c9c1"
#define BACK_COLOR "#202020"
#define DARK_BD_COLOR "#000000"
#define LIGHT_BD_COLOR "#ffffff"
#define LINE_COLOR "#71e371"
#define GEOMETRY "+0+0"
#define IMAGE ""
#define MAX_MOUSE_REGIONS 16
#define FREE(p) do {free(p); (p) = NULL;} while(0)

/*
 * Typedefs
 */
typedef struct {
    Pixmap pixmap;
    Pixmap mask;
    XpmAttributes attributes;
} xpm;

typedef struct {
    bool enable;
    int top;
    int bottom;
    int left;
    int right;
} mouse_region;

/*
 * Global variables
 */
extern Display *display;

extern char *main_color;
extern char *back_color;
extern char *dark_bd_color;
extern char *light_bd_color;
extern char *display_name;
extern char *geometry;
extern char *image;

extern unsigned scale;
extern unsigned windowed;
extern unsigned docked;

extern Atom deleteWin;

/*
 * Function Prototypes
 */
void open_window(int, char **, char **, char *, int, int);
void redraw_window(void);
void redraw_window_xy(int, int);
void copy_xpm_area(int, int, int, int, int, int);
void copy_xbm_area(int, int, int, int, int, int);
unsigned long get_color(char *, float);
unsigned long mix_color(char *, int, char *, int);
void set_foreground(int);
void fill_rectangle(int, int, int, int);
void add_mouse_region(int, int, int, int, int);
void enable_mouse_region(int);
void disable_mouse_region(int);
int check_mouse_region(XEvent);
bool execute_command(char *);
