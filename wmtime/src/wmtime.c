/*  wmtime - Window Maker dockapp that displays the time and date
    Copyright (C) 1997, 1998 Martijn Pieterse <pieterse@xs4all.nl>
    Copyright (C) 1997, 1998 Antoine Nulle <warp@xs4all.nl>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
	Code based on wmppp/wmifs

	[Orig WMMON comments]

	This code was mainly put together by looking at the
	following programs:

	asclock
		A neat piece of equip, used to display the date
		and time on the screen.
		Comes with every AfterStep installation.

		Source used:
			How do I create a not so solid window?
			How do I open a window?
			How do I use pixmaps?

	------------------------------------------------------------

	Author: Martijn Pieterse (pieterse@xs4all.nl)

	This program is distributed under the GPL license.
	(as were asclock and pppstats)

	----
	Changes:
	----
   15/07/2008 (Paul Harris, harris.pc@gmail.com)
      * Minor changes to correct build warnings
	09/10/2003 (Simon Law, sfllaw@debian.org)
		* Add -geometry support
		* Add -noseconds support
		* Make the digital clock fill the space provided
		* Eliminated exploitable static buffers
	17/05/1998 (Antoine Nulle, warp@xs4all.nl)
		* Updated version number and some other minor stuff
	16/05/1998 (Antoine Nulle, warp@xs4all.nl)
		* Added Locale support, based on original diff supplied
		  by Alen Salamun (snowman@hal9000.medinet.si)
	04/05/1998 (Martijn Pieterse, pieterse@xs4all.nl)
		* Moved the hands one pixel down.
		* Removed the redraw_window out of the main loop
	02/05/1998 (Martijn Pieterse, pieterse@xs4all.nl)
		* Removed a lot of code that was in the wmgeneral dir.
	02/05/1998 (Antoine Nulle, warp@xs4all.nl)
		* Updated master-xpm, hour dots where a bit 'off'
	30/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
		* Added anti-aliased hands
	23/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
		* Changed the hand lengths.. again! ;)
		* Zombies were created, so added wait code
	21/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
		* Added digital/analog switching support
	18/04/1998 (Martijn Pieterse, pieterse@xs4all.nl)
		* Started this project.
		* Copied the source from wmmon.
*/

#define _GNU_SOURCE
#include <X11/X.h>                     /* for ButtonPress, ButtonRelease, etc */
#include <X11/Xlib.h>                  /* for XEvent, XButtonEvent, etc */
#include <X11/xpm.h>
#include <ctype.h>                     /* for toupper */
#include <iconv.h>                     /* for iconv, iconv_close, etc */
#include <langinfo.h>                  /* for nl_langinfo, ABDAY_1, etc */
#include <locale.h>                    /* for NULL, setlocale, LC_ALL */
#include <math.h>                      /* for floor, cos, sin, M_PI */
#include <stddef.h>                    /* for size_t */
#include <stdio.h>                     /* for printf, asprintf, snprintf, etc */
#include <stdlib.h>                    /* for abs, free, exit, getenv */
#include <string.h>                    /* for strcmp, strdup, strncpy, etc */
#include <sys/wait.h>                  /* for waitpid, WNOHANG */
#include <time.h>                      /* for tm, time, localtime */
#include <unistd.h>                    /* for usleep */

#include "dockapp.h"
#include "pixmaps/master.xpm"
#include "pixmaps/master_2.xpm"
#include "pixmaps/master_3.xpm"
#include "pixmaps/master_4.xpm"
#include "pixmaps/mask.xbm"
#include "pixmaps/mask_2.xbm"
#include "pixmaps/mask_3.xbm"
#include "pixmaps/mask_4.xbm"


  /***********/
 /* Defines */
/***********/

const char* default_left_action = NULL;
const char* default_middle_action = NULL;
const char* default_right_action = NULL;


  /********************/
 /* Global Variables */
/********************/

char *locale = NULL;
char *left_action = NULL;
char *right_action = NULL;
char *middle_action = NULL;
int		digital = 0;
int		noseconds = 0;
char	day_of_week[7][3] = { "SU", "MO", "TU", "WE", "TH", "FR", "SA" };
char	mon_of_year[12][4] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

/* functions */
void usage();
void printversion(void);
void scan_args(int, char **);
void wmtime_routine(int, char **);
void get_lang();


int main(int argc, char *argv[]) {
	scan_args(argc, argv);

	if (setlocale(LC_ALL, locale) == NULL)
		fprintf(stderr, "warning: locale '%s' not recognized; defaulting to '%s'.", locale, setlocale(LC_ALL, NULL));

	get_lang();

	wmtime_routine(argc, argv);
	return 0;
}

void scan_args(int argc, char **argv) {
    int i;

    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "--display")) {
  	        display_name = argv[++i];

        } else if (!strcmp(argv[i], "--main-color")) {
	        main_color = argv[++i];

        } else if (!strcmp(argv[i], "--back-color")) {
	        back_color = argv[++i];

        } else if (!strcmp(argv[i], "--dark-color")) {
	        dark_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--light-color")) {
	        light_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--geometry")) {
	        geometry = argv[++i];

        } else if (!strcmp(argv[i], "--scale")) {
            char *tmp = argv[++i];

            if (tmp == NULL)
                scale = 1;
            else
	            scale = atoi(tmp);

	        if (scale < 1 || scale > 4) {
	            printf("Warning: scale must be >= 1 and <= 4.\n");

	            scale = 1;
	        }

        } else if (!strcmp(argv[i], "--window")) {
	        windowed = 1;

        } else if (!strcmp(argv[i], "--dock")) {
	        docked = 1;

        } else if (!strcmp(argv[i], "--image")) {
	        image = argv[++i];

        } else if (!strcmp(argv[i], "-l") || !strcmp(argv[i], "--locale")) {
	        locale = argv[++i];

        } else if (!strcmp(argv[i], "--left-action")) {
	        left_action = argv[++i];

        } else if (!strcmp(argv[i], "--middle-action")) {
	        middle_action = argv[++i];

        } else if (!strcmp(argv[i], "--right-action")) {
	        right_action = argv[++i];

        } else if (!strcmp(argv[i], "-d") || !strcmp(argv[i], "--digital")) {
	        digital = 1;

        } else if (!strcmp(argv[i], "-n") || !strcmp(argv[i], "--no-seconds")) {
	        noseconds = 1;

        } else if (!strcmp(argv[i], "-v") || !strcmp(argv[i], "--version")) {
            printversion();

            exit(1);

        } else {
            usage();

            exit(1);
        }
    }
}

/************/
/* get_lang */
/************/
void get_lang(void)
{
	char langbuf[11], outbuf[11];
	char *inp, *outp;
	iconv_t icd;
	int i, ret;
	size_t insize, outsize;

	icd = iconv_open("ASCII//TRANSLIT", nl_langinfo(CODESET));
	if (icd == (iconv_t) -1) {
		perror("wmtime: Error allocating charset conversion descriptor");
		return;
	}

	for (i = 0; i < 7; i++) {
		strncpy(langbuf, nl_langinfo(ABDAY_1 + i), 10);
		insize = outsize = 10;
		inp = langbuf;
		outp = outbuf;
		do {
			ret = iconv(icd, &inp, &insize, &outp, &outsize);
		} while (outsize > 0 && ret > 0);
		if (strstr(outbuf,"?") != NULL) return;
		for (outp = outbuf, outsize = 0; *outp != 0 && outsize < 2;
		    outp++, outsize++)
			day_of_week[i][outsize] = toupper(*outp);
	}
	for (i = 0; i < 12; i++) {
		strncpy(langbuf, nl_langinfo(ABMON_1 + i), 10);
		insize = outsize = 10;
		inp = langbuf;
		outp = outbuf;
		do {
			ret = iconv(icd, &inp, &insize, &outp, &outsize);
		} while (outsize > 0 && ret > 0);
		if (strstr(outbuf,"?") != NULL) return;
		for (outp = outbuf, outsize = 0; *outp != 0 && outsize < 3;
		    outp++, outsize++)
			mon_of_year[i][outsize] = toupper(*outp);
	}

	iconv_close(icd);
}

/*******************************************************************************\
|* wmtime_routine															   *|
\*******************************************************************************/

void DrawTime(int, int, int);
void DrawWijzer(int, int, int);
void DrawDate(int, int, int);

void wmtime_routine(int argc, char **argv) {
	int i;
	XEvent Event;
	int but_stat = -1;

	struct tm	*time_struct;

	long		starttime;
	long		curtime;

	/* Scan through ~/.wmtimerc for the mouse button actions. */
	if (default_left_action) left_action = strdup(default_left_action);
	if (default_middle_action) middle_action = strdup(default_middle_action);
	if (default_right_action) right_action = strdup(default_right_action);

	if (scale == 1)
        open_window(argc, argv, (char **)master_xpm, (char *)mask_bits, mask_width, mask_height);

    if (scale == 2)
        open_window(argc, argv, (char **)master_2_xpm, (char *)mask_2_bits, mask_2_width, mask_2_height);

    if (scale == 3)
        open_window(argc, argv, (char **)master_3_xpm, (char *)mask_3_bits, mask_3_width, mask_3_height);

    if (scale == 4)
        open_window(argc, argv, (char **)master_4_xpm, (char *)mask_4_bits, mask_4_width, mask_4_height);

	/* Mask out the right parts of the clock */
    copy_xpm_area(68, 48, 16, 12, 4, 48);  /* Draw day border */
    copy_xpm_area(86, 48, 38, 12, 22, 48); /* Draw date border */

    /* add mouse region */
	add_mouse_region(0, 5, 48, 58, 60);
	add_mouse_region(1, 5, 5, 58, 46);

	starttime = time(0);

	curtime = time(0);
	time_struct = localtime(&curtime);

	while (1) {
		curtime = time(0);

		waitpid(0, NULL, WNOHANG);

		time_struct = localtime(&curtime);


		if (curtime >= starttime) {
			if (!digital) {
				/* Now to update the seconds */

				DrawWijzer(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);

				DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);

			} else {

				DrawTime(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);

				DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);
			}
			redraw_window();
		}


		while (XPending(display)) {
			XNextEvent(display, &Event);
			switch (Event.type) {
			case Expose:
				redraw_window();
				break;
			case DestroyNotify:
				XCloseDisplay(display);
				exit(0);
				break;
		    case ClientMessage:
                if (Event.xclient.data.l[0] == (int)deleteWin) {
                    exit(0);
                }
                break;
			case ButtonPress:
				but_stat = check_mouse_region(Event);
				break;
			case ButtonRelease:
				i = check_mouse_region(Event);
				if (but_stat == i && but_stat >= 0) {
					switch (but_stat) {
					case 0:
						digital = 1-digital;

						if (digital) {
							copy_xpm_area(68, 102, 56, 42, 4, 4);
							DrawTime(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);
							DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);
						} else {
							copy_xpm_area(4, 102, 56, 42, 4, 4);
							DrawWijzer(time_struct->tm_hour, time_struct->tm_min, time_struct->tm_sec);
							DrawDate(time_struct->tm_wday, time_struct->tm_mday, time_struct->tm_mon);
						}
						redraw_window();
						break;
					case 1:
						switch (Event.xbutton.button) {
						case 1:
							if (left_action)
								execute_command(left_action);
							break;
						case 2:
							if (middle_action)
								execute_command(middle_action);
							break;
						case 3:
							if (right_action)
								execute_command(right_action);
							break;
						}
					}
				}
				break;
			}
		}

		/* Sleep 0.3 seconds */
		usleep(300000L);
	}
}

/*******************************************************************************\
|* DrawTime																	   *|
\*******************************************************************************/

void DrawTime(int hr, int min, int sec) {
#define TIME_SIZE 16
	char	time[TIME_SIZE];
	char	*p = time;
	int		i,j,k=6;
	int numfields;

	/* 7x13 */

	if (noseconds) {
		snprintf(time, TIME_SIZE, "%02d:%02d ", hr, min);
		numfields = 2;
	}
	else {
		snprintf(time, TIME_SIZE, "%02d:%02d:%02d ", hr, min, sec);
		numfields = 3;
	}

	for (i=0; i < numfields; i++) {
		for (j=0; j<2; j++) {
			copy_xpm_area((*p-'0')*7 + 1, 84, 8, 13, k, 18);
			k += 7;
			p++;
		}
		if (*p == ':') {
			copy_xpm_area(71, 84, 5, 13, k, 18);
			k += 4;
			p++;
		}
	}
}

/*******************************************************************************\
|* DrawDate																	   *|
\*******************************************************************************/

void DrawDate(int wkday, int dom, int month) {
#define DATE_SIZE 16
	char	date[DATE_SIZE];
	char	*p = date;
	int		i,k;

	/* 7x13 */

	snprintf(date, DATE_SIZE,
			"%.2s%02d%.3s  ", day_of_week[wkday], dom, mon_of_year[month]);

	k = 5;
	for (i=0; i<2; i++) {
		if (*p < 'A')
			copy_xpm_area((*p-'0')*6, 64, 6, 9, k, 49);
		else
			copy_xpm_area((*p-'A')*6, 74, 6, 9, k, 49);
		k += 6;
		p++;
	}
	k = 23;
	for (i=0; i<2; i++) {
		copy_xpm_area((*p-'0')*6, 64, 6, 9, k, 49);
		k += 6;
		p++;
	}
	copy_xpm_area(61, 64, 4, 9, k, 49);
	k += 4;
	for (i=0; i<3; i++) {
		if (*p < 'A')
			copy_xpm_area((*p-'0')*6, 64, 6, 9, k, 49);
		else
			copy_xpm_area((*p-'A')*6, 74, 6, 9, k, 49);
		k += 6;
		p++;
	}
}

/*******************************************************************************\
|* DrawWijzer																   *|
\*******************************************************************************/

void DrawWijzer(int hr, int min, int sec) {

	double		psi;
	int			dx,dy;
	int			x,y;
	int			ddx,ddy;
	int			adder;
	int			k;

	int			i;

	hr %= 12;

	copy_xpm_area(4, 102, 56, 42, 4, 4);

	/**********************************************************************/
	psi = hr * (M_PI / 6.0);
	psi += min * (M_PI / 360);

	dx = floor(sin(psi) * 22 * 0.7 + 0.5);
	dy = floor(-cos(psi) * 16 * 0.7 + 0.5);

	/* dx, dy is het punt waar we naar toe moeten.
	 * Zoek alle punten die ECHT op de lijn liggen: */

	ddx = 1;
	ddy = 1;
	if (dx < 0) ddx = -1;
	if (dy < 0) ddy = -1;

	x = 0;
	y = 0;

	if (abs(dx) > abs(dy)) {
		if (dy != 0)
			adder = abs(dx) / 2;
		else
			adder = 0;
		for (i=0; i<abs(dx); i++) {
			/* laat de kleur afhangen van de adder.
			 * adder loopt van abs(dx) tot 0 */

			k = 12 - adder / (abs(dx) / 12.0);
			copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 - ddy);

			copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

			k = 12-k;
			copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 + ddy);


			x += ddx;

			adder -= abs(dy);
			if (adder < 0) {
				adder += abs(dx);
				y += ddy;
			}
		}
	} else {
		if (dx != 0)
			adder = abs(dy) / 2;
		else
			adder = 0;
		for (i=0; i<abs(dy); i++) {
			k = 12 - adder / (abs(dy) / 12.0);
			copy_xpm_area(79+k, 67, 1, 1, x + 31 - ddx, y + 24);

			copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

			k = 12-k;
			copy_xpm_area(79+k, 67, 1, 1, x + 31 + ddx, y + 24);

			y += ddy;

			adder -= abs(dx);
			if (adder < 0) {
				adder += abs(dy);
				x += ddx;
			}
		}
	}
	/**********************************************************************/
	psi = min * (M_PI / 30.0);
	psi += sec * (M_PI / 1800);

	dx = floor(sin(psi) * 22 * 0.55 + 0.5);
	dy = floor(-cos(psi) * 16 * 0.55 + 0.5);

	/* dx, dy is het punt waar we naar toe moeten.
	 * Zoek alle punten die ECHT op de lijn liggen: */

	dx += dx;
	dy += dy;

	ddx = 1;
	ddy = 1;
	if (dx < 0) ddx = -1;
	if (dy < 0) ddy = -1;

	x = 0;
	y = 0;

	if (abs(dx) > abs(dy)) {
		if (dy != 0)
			adder = abs(dx) / 2;
		else
			adder = 0;
		for (i=0; i<abs(dx); i++) {
			/* laat de kleur afhangen van de adder.
			 * adder loopt van abs(dx) tot 0 */

			k = 12 - adder / (abs(dx) / 12.0);
			copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 - ddy);

			copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

			k = 12-k;
			copy_xpm_area(79+k, 67, 1, 1, x + 31, y + 24 + ddy);


			x += ddx;

			adder -= abs(dy);
			if (adder < 0) {
				adder += abs(dx);
				y += ddy;
			}
		}
	} else {
		if (dx != 0)
			adder = abs(dy) / 2;
		else
			adder = 0;
		for (i=0; i<abs(dy); i++) {
			k = 12 - adder / (abs(dy) / 12.0);
			copy_xpm_area(79+k, 67, 1, 1, x + 31 - ddx, y + 24);

			copy_xpm_area(79, 67, 1, 1, x + 31, y + 24);

			k = 12-k;
			copy_xpm_area(79+k, 67, 1, 1, x + 31 + ddx, y + 24);

			y += ddy;

			adder -= abs(dx);
			if (adder < 0) {
				adder += abs(dy);
				x += ddx;
			}
		}
	}
	/**********************************************************************/
	if (noseconds)
		return;    /* Skip drawing the seconds. */

	psi = sec * (M_PI / 30.0);

	dx = floor(sin(psi) * 22 * 0.9 + 0.5);
	dy = floor(-cos(psi) * 16 * 0.9 + 0.5);

	/* dx, dy is het punt waar we naar toe moeten.
	 * Zoek alle punten die ECHT op de lijn liggen: */

	ddx = 1;
	ddy = 1;
	if (dx < 0) ddx = -1;
	if (dy < 0) ddy = -1;

	if (dx == 0) ddx = 0;
	if (dy == 0) ddy = 0;

	x = 0;
	y = 0;


	if (abs(dx) > abs(dy)) {
		if (dy != 0)
			adder = abs(dx) / 2;
		else
			adder = 0;
		for (i=0; i<abs(dx); i++) {
			/* laat de kleur afhangen van de adder.
			 * adder loopt van abs(dx) tot 0 */

			k = 12 - adder / (abs(dx) / 12.0);
			copy_xpm_area(79+k, 70, 1, 1, x + 31, y + 24 - ddy);

			k = 12-k;
			copy_xpm_area(79+k, 70, 1, 1, x + 31, y + 24);


			x += ddx;

			adder -= abs(dy);
			if (adder < 0) {
				adder += abs(dx);
				y += ddy;
			}
		}
	} else {
		if (dx != 0)
			adder = abs(dy) / 2;
		else
			adder = 0;
		for (i=0; i<abs(dy); i++) {
			k = 12 - adder / (abs(dy) / 12.0);
			copy_xpm_area(79+k, 70, 1, 1, x + 31 - ddx, y + 24);

			k = 12-k;
			copy_xpm_area(79+k, 70, 1, 1, x + 31, y + 24);

			y += ddy;

			adder -= abs(dx);
			if (adder < 0) {
				adder += abs(dy);
				x += ddx;
			}
		}
	}
}

/*******************************************************************************\
|* usage																	   *|
\*******************************************************************************/

void usage() {
	printf("Usage: %s [OPTION]...\n", NAME);
	printf("WindowMaker dockapp that displays the time and date.\n");
	printf("\n");
	printf("  -d --digital                  Display the digital clock.\n");
	printf("  -n --noseconds                Disables the second hand.\n");
	printf("  -l --locale         LOCALE    Set locale to LOCALE.\n");
	printf("     --left-action    ACTION    Set left mouse button action.\n");
	printf("     --middle-action  ACTION    Set middle mouse button action.\n");
	printf("     --right-action   ACTION    Set right mouse button action.\n");
	printf("     --display        DISPLAY   Use alternate X display.\n");
    printf("     --main-color     COLOR     Set main color.\n");
    printf("     --back-color     COLOR     Set background color.\n");
    printf("     --dark-color     COLOR     Set border dark color.\n");
    printf("     --light-color    COLOR     Set border light color.\n");
    printf("     --geometry       GEOMETRY  Set window geometry (position).\n");
    printf("     --scale          SCALE     Set dockapp scale (from 1 to 4).\n");
    printf("     --window                   Run in the windowed mode.\n");
    printf("     --dock                     In the windowed mode, run as a panel.\n");
    printf("     --image          IMAGE     Set background xpm image.\n");
	printf("  -h --help                     Display this help and exit.\n");
	printf("  -v --version                  Output version information and exit.\n");
	printf("\n");
}

/*******************************************************************************\
|* printversion																   *|
\*******************************************************************************/

void printversion(void) {
	printf("WMTime version %s\n", VERSION);
}

/* vim: sw=4 ts=4 columns=82
 */
