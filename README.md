# dockapps

This is a collection of scalable dockapps, the size can be changed from the old 64x64 classic to 256x256 px. As of now, Window Maker's git NEXT branch has the support for dockapp larger than 64x64 px.

Run a dockapp with the --scale parameter and set a value from 1 to 4. Floating point numbers or values beyond the limit will not work. In case you need 64x64, you can just run them without the --scale option or use vanilla dockapps.

These dockapps do not depend on any libdockapp, they also do not read config files and can only be configured via command line options. Run a dockapp with the -h or --help option to see a full set of parameters.

More dockapps to be added later.
