#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <stdarg.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/xpm.h>
#include <X11/extensions/shape.h>

#define NAME "asmon"
#define VERSION "0.73"
#define MAIN_COLOR "#00c9c1"
#define BACK_COLOR "#202020"
#define DARK_BD_COLOR "#000000"
#define LIGHT_BD_COLOR "#ffffff"
#define GEOMETRY "+0+0"
#define IMAGE ""
#define FREE(p) do {free(p); (p) = NULL;} while(0)

/*
 * Typedefs
 */
typedef struct {
    Pixmap pixmap;
    Pixmap mask;
    XpmAttributes attributes;
} xpm;

/*
 * Global variables
 */
extern Display *display;

extern char *main_color;
extern char *back_color;
extern char *dark_bd_color;
extern char *light_bd_color;
extern char *display_name;
extern char *geometry;
extern char *image;

extern int scale;

extern unsigned windowed;
extern unsigned docked;

extern Atom deleteWin;

/*
 * Function Prototypes
 */
void open_window(int, char **, char **, char *, int, int);
void redraw_window(void);
void redraw_window_xy(int, int);
void copy_xpm_area(int, int, int, int, int, int);
void copy_xbm_area(int, int, int, int, int, int);
unsigned long get_color(char *, float);
unsigned long mix_color(char *, int, char *, int);
void set_foreground(int);
void fill_rectangle(int, int, int, int);
