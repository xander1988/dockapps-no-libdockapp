/*
 *   xutils.c - A collection of X-windowdows utilties for creating WindowMAker
 *		DockApps.
 *
 *     This file contains alot of the lower-level X windowdows routines. Origins with wmppp
 *     (by  Martijn Pieterse (pieterse@xs4all.nl)), but its been hacked up quite a bit
 *     and passed on from one new DockApp to the next.
 *
 *
 *
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2, or (at your option)
 *      any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program (see the file COPYING); if not, write to the
 *      Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *      Boston, MA 02110-1301 USA
 *
 *
 * $Id: xutils.c,v 1.2 2002/09/15 14:31:41 ico Exp $
 *
 *
 */

#include "dockapp.h"

Display *display;

Window root_window;
Window icon_window, window;

int screen;
int depth;

int scale = 1;

unsigned windowed = 0;
unsigned docked = 0;

unsigned long xcolor[7];

Atom _XA_GNUSTEP_WM_FUNC;
Atom deleteWin;
Atom winType;
Atom dockWin;

/*
 * X11 variables
 */
int x_fd;

XSizeHints mysizehints;

XWMHints mywmhints;

Pixel back_pix, fore_pix;

char *main_color = MAIN_COLOR;
char *back_color = BACK_COLOR;
char *dark_bd_color = DARK_BD_COLOR;
char *light_bd_color = LIGHT_BD_COLOR;
char *line_color = LINE_COLOR;
char *display_name = NULL;
char *geometry = GEOMETRY;
char *image = IMAGE;

GC NormalGC;

xpm wmgen;

Pixmap pixmask;
Pixmap pixmap_tile, pixmask_tile;

mouse_region mr[MAX_MOUSE_REGIONS];

volatile bool busy;

/*
 * flush_expose
 */
static int flush_expose(Window w) {
    XEvent dummy;

    int i = 0;

    while (XCheckTypedWindowEvent(display, w, Expose, &dummy))
        i++;

    return i;
}

/*
 * RedrawWindow
 * RedrawWindowXY
 */
void redraw_window(void) {
    if (!docked) {
        flush_expose(icon_window);

        XCopyArea(display, wmgen.pixmap, icon_window, NormalGC, 0, 0, wmgen.attributes.width * scale, wmgen.attributes.height * scale, 0, 0);
    }

    flush_expose(window);

    XCopyArea(display, wmgen.pixmap, window, NormalGC, 0, 0, wmgen.attributes.width * scale, wmgen.attributes.height * scale, 0, 0);
}

void redraw_window_xy(int x, int y) {
    if (!docked) {
        flush_expose(icon_window);

        XCopyArea(display, wmgen.pixmap, icon_window, NormalGC, x * scale, y * scale, wmgen.attributes.width * scale, wmgen.attributes.height * scale, 0, 0);
    }

    flush_expose(window);

    XCopyArea(display, wmgen.pixmap, window, NormalGC, x * scale, y * scale, wmgen.attributes.width * scale, wmgen.attributes.height * scale, 0, 0);
}

/*
 * copyXPMArea
 * copyXBMArea
 */
void copy_xpm_area(int x, int y, int sx, int sy, int dx, int dy) {
	XCopyArea(display, wmgen.pixmap, wmgen.pixmap, NormalGC, x * scale, y * scale, sx * scale, sy * scale, dx * scale, dy * scale);
}

void copy_xbm_area(int x, int y, int sx, int sy, int dx, int dy) {
	XCopyArea(display, wmgen.mask, wmgen.pixmap, NormalGC, x * scale, y * scale, sx * scale, sy * scale, dx * scale, dy * scale);
}

/*
 * openXwindow
 */
void open_window(int argc, char *argv[], char *pixmap_bytes[], char *pixmask_bits, int pixmask_width, int pixmask_height) {
    unsigned int borderwidth = 1;

    XClassHint classHint;

    char *wname = argv[0];

    XTextProperty name;

    XGCValues gcv;

    unsigned long gcm;

    int dummy = 0;

    if (!(display = XOpenDisplay(display_name))) {
        fprintf(stderr, "%s: can't open display %s\n", argv[0], XDisplayName(display_name));

        exit(1);
    }

    screen = DefaultScreen(display);

    root_window = RootWindow(display, screen);

    depth = DefaultDepth(display, screen);

    x_fd = XConnectionNumber(display);

    _XA_GNUSTEP_WM_FUNC = XInternAtom(display, "_GNUSTEP_WM_FUNCTION", false);
    deleteWin = XInternAtom(display, "WM_DELETE_WINDOW", false);
    winType = XInternAtom(display, "_NET_WM_WINDOW_TYPE", false);
    dockWin = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DOCK", false);

    xcolor[0] = get_color(back_color, 1.0);
    xcolor[1] = mix_color(main_color, 100, back_color, 0);
    xcolor[2] = mix_color(main_color, 60,  back_color, 40);
    xcolor[3] = mix_color(main_color, 25,  back_color, 75);
    xcolor[4] = get_color(dark_bd_color, 1.0);
    xcolor[5] = get_color(light_bd_color, 1.0);
    xcolor[6] = get_color(line_color, 1.0);

    XpmColorSymbol colors[7] = {
        {"back_color",     NULL, xcolor[0]},
        {"led_color_high", NULL, xcolor[1]},
        {"led_color_med",  NULL, xcolor[2]},
        {"led_color_low",  NULL, xcolor[3]},
        {"border_dark",    NULL, xcolor[4]},
        {"border_light",   NULL, xcolor[5]},
        {"line_color",     NULL, xcolor[6]}
    };

    wmgen.attributes.numsymbols   = 7;
    wmgen.attributes.colorsymbols = colors;
    wmgen.attributes.exactColors  = False;
    wmgen.attributes.closeness    = 40000;
    wmgen.attributes.valuemask    = XpmReturnPixels | XpmReturnExtensions | XpmColorSymbols | XpmExactColors | XpmCloseness | XpmSize;

    if (XpmCreatePixmapFromData(display, root_window, pixmap_bytes, &(wmgen.pixmap), &(wmgen.mask), &(wmgen.attributes)) != XpmSuccess) {
	    fprintf(stderr, "Not enough free colorcells.\n");

	    exit(1);
    }

    /*
     * Create a window
     */
    mysizehints.flags = USSize | USPosition;
    mysizehints.x = 0;
    mysizehints.y = 0;

    back_pix = get_color("white", 1.0);
    fore_pix = get_color("black", 1.0);

    XWMGeometry(display, screen, geometry, NULL, borderwidth, &mysizehints, &mysizehints.x, &mysizehints.y, &mysizehints.width, &mysizehints.height, &dummy);

    mysizehints.width = 64 * scale;
    mysizehints.height = 64 * scale;

    window = XCreateSimpleWindow(display, root_window, mysizehints.x, mysizehints.y, mysizehints.width, mysizehints.height, borderwidth, fore_pix, back_pix);

    if (!docked)
        icon_window = XCreateSimpleWindow(display, window, mysizehints.x, mysizehints.y, mysizehints.width, mysizehints.height, borderwidth, fore_pix, back_pix);

    /*
     * Activate hints
     */
    if (docked) {
        XChangeProperty(display, window, winType, XA_ATOM, 32, PropModeReplace, (unsigned char *)&dockWin, 1);
    }

    classHint.res_name = wname;
    classHint.res_class = wname;

    XSetClassHint(display, window, &classHint);

    /*
     * Set up the xevents that you want the relevent windows to inherit
     * Currently, its seems that setting KeyPress events here has no
     * effect. I.e. for some you will need to Grab the focus and then return
     * it after you are done...
     */
    XSelectInput(display, window, ButtonPressMask | ExposureMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask | EnterWindowMask | LeaveWindowMask | KeyPressMask | KeyReleaseMask);

    if (!docked)
        XSelectInput(display, icon_window, ButtonPressMask | ExposureMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask | EnterWindowMask | LeaveWindowMask | KeyPressMask | KeyReleaseMask);

    if (XStringListToTextProperty(&wname, 1, &name) == 0) {
        fprintf(stderr, "%s: can't allocate windowdow name\n", wname);

        exit(1);
    }

    XSetWMName(display, window, &name);

    /*
     * Create Graphics Context (GC) for drawing
     */
    gcm = GCForeground | GCBackground | GCGraphicsExposures;

    gcv.foreground = fore_pix;
    gcv.background = back_pix;
    gcv.graphics_exposures = 0;

    NormalGC = XCreateGC(display, root_window, gcm, &gcv);

    pixmask = XCreateBitmapFromData(display, window, pixmask_bits, pixmask_width, pixmask_height);

    XShapeCombineMask(display, window, ShapeBounding, 0, 0, pixmask, ShapeSet);

    if (!docked)
        XShapeCombineMask(display, icon_window, ShapeBounding, 0, 0, pixmask, ShapeSet);

    if (windowed) {
        if (strlen(image)) {
            if (!XpmReadFileToPixmap(display, root_window, image, &pixmap_tile, &pixmask_tile, &wmgen.attributes)) {
                XCopyArea(display, pixmap_tile, wmgen.pixmap, NormalGC, 0, 0, 64 * scale, 64 * scale, 0, 0);

                XShapeCombineMask(display, window, ShapeBounding, 0, 0, pixmask_tile, ShapeSet);

            } else {
                printf("WARN: failed to set the \"%s\" background image!\n", image);

                XShapeCombineMask(display, window, ShapeBounding, 0, 0, pixmask, ShapeSet);
            }

        } else {
            XShapeCombineMask(display, window, ShapeBounding, 0, 0, pixmask, ShapeSet);
        }

        mywmhints.initial_state = NormalState;
        mywmhints.window_group  = window;
        mywmhints.flags         = StateHint;

        /*XSetClipMask(display, NormalGC, pixmask);

        XCopyArea(display, wmgen.pixmap, window, NormalGC, 0, 0, pixmask_width, pixmask_height, 0, 0);

        XSetClipMask(display, NormalGC, None);*/

    } else {
        mywmhints.initial_state = WithdrawnState;
        mywmhints.icon_window   = icon_window;
        mywmhints.icon_x        = mysizehints.x;
        mywmhints.icon_y        = mysizehints.y;
        mywmhints.window_group  = window;
        mywmhints.flags         = StateHint | IconWindowHint | IconPositionHint | WindowGroupHint;
    }

    XSetWMHints(display, window, &mywmhints);

    XSetWMNormalHints(display, window, &mysizehints);

    XSetCommand(display, window, argv, argc);

    XSetWMProtocols(display, window, &deleteWin, 1);

    XMapWindow(display, window);
}

unsigned long get_color(char *ColorName, float fac) {
     XColor Color;

     XWindowAttributes Attributes;

     XGetWindowAttributes(display, root_window, &Attributes);

     Color.pixel = 0;

     XParseColor(display, Attributes.colormap, ColorName, &Color);

     Color.red 	 = (unsigned short)(Color.red / fac);
     Color.blue  = (unsigned short)(Color.blue / fac);
     Color.green = (unsigned short)(Color.green / fac);
     Color.flags = DoRed | DoGreen | DoBlue;

     XAllocColor(display, Attributes.colormap, &Color);

     return Color.pixel;
}

unsigned long mix_color(char *colorname1, int prop1, char *colorname2, int prop2) {
    XColor color, color1, color2;

    XWindowAttributes winattr;

    XGetWindowAttributes(display, root_window, &winattr);

    XParseColor(display, winattr.colormap, colorname1, &color1);
    XParseColor(display, winattr.colormap, colorname2, &color2);

    color.pixel = 0;
    color.red = (color1.red * prop1 + color2.red * prop2) / (prop1 + prop2);
    color.green = (color1.green * prop1 + color2.green * prop2) / (prop1 + prop2);
    color.blue = (color1.blue * prop1 + color2.blue * prop2) / (prop1 + prop2);
    color.flags = DoRed | DoGreen | DoBlue;

    XAllocColor(display, winattr.colormap, &color);

    return color.pixel;
}

void set_foreground(int number) {
    XSetForeground(display, NormalGC, xcolor[number]);
}

void fill_rectangle(int x, int y, int w, int h) {
    XFillRectangle(display, wmgen.pixmap, NormalGC, x * scale, y * scale, w * scale, h * scale);
}

void add_mouse_region(int index, int left, int top, int right, int bottom) {
    if (index < MAX_MOUSE_REGIONS) {
        mr[index].enable = true;
        mr[index].top = top * scale;
        mr[index].left = left * scale;
        mr[index].bottom = bottom * scale;
        mr[index].right = right * scale;
    }
}

void enable_mouse_region(int index) {
    if (index < MAX_MOUSE_REGIONS) {
        mr[index].enable = true;
    }
}

void disable_mouse_region(int index) {
    if (index < MAX_MOUSE_REGIONS) {
        mr[index].enable = false;
    }
}

int check_mouse_region(XEvent event) {
    int i;
    int found;
    int x, y;

    x = event.xbutton.x;
    y = event.xbutton.y;

    found = 0;

    for (i = 0; i < MAX_MOUSE_REGIONS && !found; i++) {
        if (mr[i].enable &&
            x <= mr[i].right &&
            x >= mr[i].left &&
            y <= mr[i].bottom &&
            y >= mr[i].top)
            found = 1;
    }

    if (!found)
        return -1;

    return (i - 1);
}

static void sigchldhandler(int a) {
    (void)a;

    wait(NULL);

    busy = false;
}

bool execute_command(char *command) {
    pid_t pid;

    signal(SIGCHLD, sigchldhandler);

    if (busy) {
        return true;
    }

    busy = true;

    if ((pid = fork()) == 0) {
        if (system(command) == -1) {
            return false;
        }

        exit(10);
    }

    return true;
}
