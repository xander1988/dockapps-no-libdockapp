/* wmpulse (based on wmamixer and pavolume) - A PulseAudio mixer designed for WindowMaker with scrollwheel support
 * Copyright (C) 2021  Xander
 * Copyright (C) 2015  Roman Dobosz <gryf@vimja.com>
 * Copyright (C) 2003  Damian Kramer <psiren@hibernaculum.net>
 * Copyright (C) 1998  Sam Hawker <shawkie@geocities.com>
 *
 * This software comes with ABSOLUTELY NO WARRANTY
 * This software is free software, and you are welcome to redistribute it
 * under certain conditions
 * See the README file for a more complete notice.
 */

#include <unistd.h>                    /* for usleep */
#include <math.h>                      /* for round */
#include <pulse/pulseaudio.h>

#include "dockapp.h"
#include "pixmaps/master.xpm"
#include "pixmaps/master_2.xpm"
#include "pixmaps/master_3.xpm"
#include "pixmaps/master_4.xpm"
#include "pixmaps/mask.xbm"
#include "pixmaps/mask_2.xbm"
#include "pixmaps/mask_3.xbm"
#include "pixmaps/mask_4.xbm"

char default_sink_name[256];
char default_source_name[256];

int retval = EXIT_SUCCESS;
int cursink;
int cursource;
/*int cursinkleft;
int cursinkright;
int cursourceleft;
int cursourceright;*/

pa_mainloop *mainloop = NULL;
pa_mainloop_api *mainloop_api = NULL;
pa_context *context = NULL;

bool dragging = false;
bool sink_muted;
bool source_muted;

typedef struct Command {
    bool is_delta_volume;
    bool is_mute_off;
    bool is_mute_on;
    bool is_mute_toggle;
    int volume;
} Command;

void usage(void);
void scan_args(int, char **);
void routine(int, char **);
void checkVol(bool forced);
/*void drawStereo(bool left);*/
void drawMono();
void drawVolLevel();
void motionEvent(XMotionEvent *xev);
void pressEvent(XButtonEvent *xev);
void releaseEvent();
void scanArgs(int argc, char **argv);
void update();
void wait_loop(pa_operation *op);
int constrain_volume(int volume);
int normalize(pa_volume_t volume);
pa_volume_t denormalize(int volume);
void set_sink_volume(pa_context *c, const pa_sink_info *i, __attribute__((unused)) int eol, void *userdata);
void set_source_volume(pa_context *c, const pa_source_info *i, __attribute__((unused)) int eol, void *userdata);
void get_server_info(__attribute__((unused)) pa_context *c, const pa_server_info *i, __attribute__((unused)) void *userdata);
void get_sink_volume(__attribute__((unused)) pa_context *c, const pa_sink_info *i, __attribute__((unused)) int eol, void *userdata);
void get_source_volume(__attribute__((unused)) pa_context *c, const pa_source_info *i, __attribute__((unused)) int eol, void *userdata);
int init_context(pa_context *c, int retval);
int quit(int new_retval);
int mixer_init();
int mixer_get_volume(bool is_source);
void mixer_set_volume(int volume, bool is_source);
void mixer_toggle_mute(bool is_source);
bool mixer_is_muted(bool is_source);

int main(int argc, char *argv[]) {
    scan_args(argc, argv);

    if (mixer_init() == 0) {
        fprintf(stderr, "Sorry, connection to server failed.\n");

        return 1;

    } else {
        routine(argc, argv);
    }

    return 0;
}

void routine(int argc, char *argv[]) {
    if (scale == 1)
        open_window(argc, argv, (char **)master_xpm, (char *)mask_bits, mask_width, mask_height);

    if (scale == 2)
        open_window(argc, argv, (char **)master_2_xpm, (char *)mask_2_bits, mask_2_width, mask_2_height);

    if (scale == 3)
        open_window(argc, argv, (char **)master_3_xpm, (char *)mask_3_bits, mask_3_width, mask_3_height);

    if (scale == 4)
        open_window(argc, argv, (char **)master_4_xpm, (char *)mask_4_bits, mask_4_width, mask_4_height);

    /* Draw borders */
    copy_xpm_area(67, 4, 27, 11, 4, 4);
    copy_xpm_area(67, 4, 27, 11, 33, 4);
    copy_xpm_area(67, 17, 27, 43, 4, 17);
    copy_xpm_area(67, 17, 27, 43, 33, 17);
    
    checkVol(true);

    XEvent xev;

    /* main loop */
    while (1) {
        while (XPending(display)) {
            XNextEvent(display, &xev);

            switch (xev.type) {
            case Expose:
                redraw_window();
                break;

            case ButtonPress:
                pressEvent(&xev.xbutton);
                break;

            case ButtonRelease:
                releaseEvent();
                break;

            case MotionNotify:
                motionEvent(&xev.xmotion);
                break;

            case DestroyNotify:
                quit(EXIT_SUCCESS);

                exit(0);

            case ClientMessage:
                if (xev.xclient.data.l[0] == (int)deleteWin) {
                    exit(0);
                }
                break;
            }
        }

        checkVol(false);

        usleep(50000);
    }
}

void wait_loop(pa_operation *op) {
    while (pa_operation_get_state(op) == PA_OPERATION_RUNNING) {
        if (pa_mainloop_iterate(mainloop, 1, &retval) < 0) {
            break;
        };
    }

    pa_operation_unref(op);
}

int constrain_volume(int volume) {
    if (volume > 100) {
        return 100;
    }

    if (volume < 0) {
        return 0;
    }

    return volume;
}

int normalize(pa_volume_t volume) {
    return (int) round(volume * 100.0 / PA_VOLUME_NORM);
}

pa_volume_t denormalize(int volume) {
    return (pa_volume_t) round(volume * PA_VOLUME_NORM / 100);
}

void set_sink_volume(pa_context *c, const pa_sink_info *i, __attribute__((unused)) int eol, void *userdata) {
    if (i == NULL) {
        return;
    }

    Command *command = (Command *) userdata;

    if (command->is_mute_on) {
        pa_context_set_sink_mute_by_index(c, i->index, 1, NULL, NULL);
    }
    if (command->is_mute_off) {
        pa_context_set_sink_mute_by_index(c, i->index, 0, NULL, NULL);
    }
    if (command->is_mute_toggle) {
        pa_context_set_sink_mute_by_index(c, i->index, !i->mute, NULL, NULL);
    }
    if (command->volume == -1 && !command->is_delta_volume) {
        return;
    }

    // Turn muting off on any volume change, unless muting was specifically turned on or toggled.
    if (!command->is_mute_on && !command->is_mute_toggle) {
        pa_context_set_sink_mute_by_index(c, i->index, 0, NULL, NULL);
    }

    pa_cvolume *cvolume = (pa_cvolume *) &i->volume;

    int new_volume = command->is_delta_volume ? normalize(pa_cvolume_avg(cvolume)) + command->volume : command->volume;

    pa_cvolume *new_cvolume = pa_cvolume_set(cvolume, i->volume.channels, denormalize(constrain_volume(new_volume)));

    pa_context_set_sink_volume_by_index(c, i->index, new_cvolume, NULL, NULL);
}

void set_source_volume(pa_context *c, const pa_source_info *i, __attribute__((unused)) int eol, void *userdata) {
    if (i == NULL) {
        return;
    }

    Command *command = (Command *) userdata;

    if (command->is_mute_on) {
        pa_context_set_source_mute_by_index(c, i->index, 1, NULL, NULL);
    }
    if (command->is_mute_off) {
        pa_context_set_source_mute_by_index(c, i->index, 0, NULL, NULL);
    }
    if (command->is_mute_toggle) {
        pa_context_set_source_mute_by_index(c, i->index, !i->mute, NULL, NULL);
    }
    if (command->volume == -1 && !command->is_delta_volume) {
        return;
    }

    // Turn muting off on any volume change, unless muting was specifically turned on or toggled.
    if (!command->is_mute_on && !command->is_mute_toggle) {
        pa_context_set_source_mute_by_index(c, i->index, 0, NULL, NULL);
    }

    pa_cvolume *cvolume = (pa_cvolume *) &i->volume;

    int new_volume = command->is_delta_volume ? normalize(pa_cvolume_avg(cvolume)) + command->volume : command->volume;

    pa_cvolume *new_cvolume = pa_cvolume_set(cvolume, i->volume.channels, denormalize(constrain_volume(new_volume)));

    pa_context_set_source_volume_by_index(c, i->index, new_cvolume, NULL, NULL);
}

void get_server_info(__attribute__((unused)) pa_context *c, const pa_server_info *i, __attribute__((unused)) void *userdata) {
    if (i == NULL) {
        return;
    }

    strncpy(default_sink_name, (char *) i->default_sink_name, 255);
    strncpy(default_source_name, (char *) i->default_source_name, 255);
}

void get_sink_volume(__attribute__((unused)) pa_context *c, const pa_sink_info *i, __attribute__((unused)) int eol, void *userdata) {
    if (i == NULL) {
        return;
    }

    Command *command = (Command *) userdata;

    if (i->mute) {
        command->is_mute_on = true;
    }

    command->volume = normalize(pa_cvolume_avg(&(i->volume)));
}

void get_source_volume(__attribute__((unused)) pa_context *c, const pa_source_info *i, __attribute__((unused)) int eol, void *userdata) {
    if (i == NULL) {
        return;
    }

    Command *command = (Command *) userdata;

    if (i->mute) {
        command->is_mute_on = true;
    }

    command->volume = normalize(pa_cvolume_avg(&(i->volume)));
}

int init_context(pa_context *c, int retval) {
    pa_context_connect(c, NULL, PA_CONTEXT_NOFLAGS, NULL);

    pa_context_state_t state;

    while (state = pa_context_get_state(c), true) {
        if (state == PA_CONTEXT_READY) {
            return 0;
        }
        if (state == PA_CONTEXT_FAILED) {
            return 1;
        }

        pa_mainloop_iterate(mainloop, 1, &retval);
    }
}

int quit(int new_retval) {
    // Only set `retval` if it hasn't been changed elsewhere (such as by PulseAudio in `pa_mainloop_iterate()`).
    if (retval == EXIT_SUCCESS) {
        retval = new_retval;
    }

    if (context) {
        pa_context_unref(context);
    }

    if (mainloop_api) {
        mainloop_api->quit(mainloop_api, retval);
    }

    if (mainloop) {
        pa_signal_done();
        pa_mainloop_free(mainloop);
    }

    return retval;
}

int mixer_init() {
    mainloop = pa_mainloop_new();

    if (!mainloop) {
        fprintf(stderr, "Could not create PulseAudio main loop\n");

        return 0;
    }

    mainloop_api = pa_mainloop_get_api(mainloop);

    if (pa_signal_init(mainloop_api) != 0) {
        fprintf(stderr, "Could not initialize PulseAudio UNIX signal subsystem\n");

        return 0;
    }

    context = pa_context_new(mainloop_api, NAME);

    if (!context || init_context(context, retval) != 0) {
        fprintf(stderr, "Could not initialize PulseAudio context\n");

        return 0;
    }

    wait_loop(pa_context_get_server_info(context, get_server_info, NULL));

    return 1;
}

int mixer_get_volume(bool is_source) {
    Command command = {
        .is_delta_volume = false,
        .is_mute_off = false,
        .is_mute_on = false,
        .is_mute_toggle = false,
        .volume = -1,
    };

    if (is_source) {
        wait_loop(pa_context_get_source_info_by_name(context, (char *) default_source_name, get_source_volume, &command));
    } else {
        wait_loop(pa_context_get_sink_info_by_name(context, (char *) default_sink_name, get_sink_volume, &command));
    }

    return command.volume;
}

void mixer_set_volume(int volume, bool is_source) {
    Command command = {
        .is_delta_volume = false,
        .is_mute_off = false,
        .is_mute_on = false,
        .is_mute_toggle = false,
        .volume = -1,
    };

    command.volume = volume;

    if (is_source) {
        wait_loop(pa_context_get_source_info_by_name(context, (char *) default_source_name, set_source_volume, &command));
    } else {
        wait_loop(pa_context_get_sink_info_by_name(context, (char *) default_sink_name, set_sink_volume, &command));
    }
}

void mixer_toggle_mute(bool is_source) {
    Command command = {
        .is_delta_volume = false,
        .is_mute_off = false,
        .is_mute_on = false,
        .is_mute_toggle = false,
        .volume = -1,
    };

    command.is_mute_toggle = true;

    if (is_source) {
        wait_loop(pa_context_get_source_info_by_name(context, (char *) default_source_name, set_source_volume, &command));
    } else {
        wait_loop(pa_context_get_sink_info_by_name(context, (char *) default_sink_name, set_sink_volume, &command));
    }
}

bool mixer_is_muted(bool is_source) {
    Command command = {
        .is_delta_volume = false,
        .is_mute_off = false,
        .is_mute_on = false,
        .is_mute_toggle = false,
        .volume = -1,
    };

    if (is_source) {
        wait_loop(pa_context_get_source_info_by_name(context, (char *) default_source_name, get_source_volume, &command));
    } else {
        wait_loop(pa_context_get_sink_info_by_name(context, (char *) default_sink_name, get_sink_volume, &command));
    }

    return command.is_mute_on;
}

void checkVol(bool forced) {
    int sink_new = mixer_get_volume(false);
    int source_new = mixer_get_volume(true);

    bool sink_muted_new = mixer_is_muted(false);
    bool source_muted_new = mixer_is_muted(true);

    if (forced) {
        cursink = sink_new;
        cursource = source_new;

        sink_muted = sink_muted_new;
        source_muted = source_muted_new;

        update();
        drawVolLevel();
        redraw_window();
    } else {
        if (sink_new != cursink) {
            cursink = sink_new;

            update();
            drawVolLevel();
            redraw_window();
        }

        if (source_new != cursource) {
            cursource = source_new;

            update();
            drawVolLevel();
            redraw_window();
        }

        if (sink_muted_new != sink_muted) {
            sink_muted = sink_muted_new;

            drawVolLevel();
            redraw_window();
        }

        if (source_muted_new != source_muted) {
            source_muted = source_muted_new;

            drawVolLevel();
            redraw_window();
        }
    }
}

void pressEvent(XButtonEvent *xev) {
    int inc, x, y, v;

    x = xev->x - ((64*scale) / 2 - (32*scale));
    y = xev->y - ((64*scale) / 2 - (32*scale));

    if (xev->button == Button4 || xev->button == Button5) {
        if (xev->button == Button4)
            inc = 4;
        else
            inc = -4;

        /* sink */
        if (x >= 6*scale && x <= 28*scale)
            mixer_set_volume(constrain_volume(mixer_get_volume(false) + inc), false);
        /* source */
        if (x >= 36*scale && x <= 58*scale)
            mixer_set_volume(constrain_volume(mixer_get_volume(true) + inc), true);

        checkVol(false);

        return;
    }

    if (x >= 6*scale && x <= 58*scale && y >= 18*scale && y <= 58*scale) {
        v = ((58*scale - y) * 100) / (40*scale);

        dragging = true;

        /* sink */
        if (x >= 6*scale && x <= 28*scale)
            mixer_set_volume(v, false);
        /* source */
        if (x >= 36*scale && x <= 58*scale)
            mixer_set_volume(v, true);

        checkVol(false);

        return;
    }

    if (x >= 5*scale && y >= 5*scale && x <= 59*scale && y <= 14*scale) {
        /* sink */
        if (x >= 5*scale && x <= 30*scale)
            mixer_toggle_mute(false);
        /* source */
        if (x >= 34*scale && x <= 59*scale)
            mixer_toggle_mute(true);

        checkVol(false);

        return;
    }
}

void releaseEvent() {
    dragging = false;

    redraw_window();
}

void motionEvent(XMotionEvent *xev) {
    int x = xev->x - ((64*scale) / 2 - (32*scale));
    int y = xev->y - ((64*scale) / 2 - (32*scale));

    if (x >= 6*scale && x <= 58*scale && y >= 18*scale && dragging) {
        int v = ((58*scale - y) * 100) / (40*scale);

        if (v < 0)
            v = 0;

        /* sink */
        if (x >= 6*scale && x <= 28*scale)
            mixer_set_volume(v, false);
        /* source */
        if (x >= 36*scale && x <= 58*scale)
            mixer_set_volume(v, true);

        checkVol(false);
    }
}

void update() {
    //if (unlink_channels) {
    //    drawStereo(true);
    //    drawStereo(false);
    //} else {
        drawMono();
    //}
}

void drawVolLevel() {
    int i;
    int digits_sin[4];
    int digits_src[4];

    int sink_vol = mixer_get_volume(false);
    int source_vol = mixer_get_volume(true);

    digits_sin[0] = (sink_vol / 100) ? 1 : 10;
    digits_sin[1] = (sink_vol / 10) == 10 ? 0 : (sink_vol / 10);
    digits_sin[2] = sink_vol % 10;
    digits_sin[3] = 10;

    digits_src[0] = (source_vol / 100) ? 1 : 10;
    digits_src[1] = (source_vol / 10) == 10 ? 0 : (source_vol / 10);
    digits_src[2] = source_vol % 10;
    digits_src[3] = 10;

    /* sink */
    if (sink_muted) {
        copy_xpm_area(126, 7, 23, 7, 6, 6);
    } else {
        for (i = 0; i < 4; i++) {
            copy_xpm_area(126 + (6 * digits_sin[i]), 0, 6, 7, 6 + (i * 5) + i, 6);
        }
    }
    /* source */
    if (source_muted) {
        copy_xpm_area(126, 7, 23, 7, 35, 6);
    } else {
        for (i = 0; i < 4; i++) {
            copy_xpm_area(126 + (6 * digits_src[i]), 0, 6, 7, 35 + (i * 5) + i, 6);
        }
    }
}

/*void drawStereo(bool left) {
    int i;
    short pos = left ? 37 : 48;

    XSetForeground(d_display, gc_gc, color[0]);
    XFillRectangle(d_display, pm_disp, gc_gc, 46, 7, 2, 49);

    XSetForeground(d_display, gc_gc, color[1]);
    for (i = 0; i < 25; i++) {
        if (i == ((left ? curleft : curright) * 25) / 100)
            XSetForeground(d_display, gc_gc, color[3]);
        XFillRectangle(d_display, pm_disp, gc_gc, pos, 55 - 2 * i, 9, 1);
    }
}*/

void drawMono() {
    int i;

    /* sink */
    set_foreground(1);

    for (i = 0; i < 20; i++) {
        if (i == (cursink * 20) / 100)
            set_foreground(3);

        fill_rectangle(6, 57 - 2 * i, 23, 1);
    }
    /* source */
    set_foreground(1);

    for (i = 0; i < 20; i++) {
        if (i == (cursource * 20) / 100)
            set_foreground(3);

        fill_rectangle(35, 57 - 2 * i, 23, 1);
    }
}

void scan_args(int argc, char **argv) {
    int i;

    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "--display")) {
  	        display_name = argv[++i];

        } else if (!strcmp(argv[i], "--main-color")) {
	        main_color = argv[++i];

        } else if (!strcmp(argv[i], "--back-color")) {
	        back_color = argv[++i];

        } else if (!strcmp(argv[i], "--dark-color")) {
	        dark_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--light-color")) {
	        light_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--geometry")) {
	        geometry = argv[++i];

        } else if (!strcmp(argv[i], "--scale")) {
            char *tmp = argv[++i];

            if (tmp == NULL)
                scale = 1;
            else
	            scale = atoi(tmp);

	        if (scale < 1 || scale > 4) {
	            printf("Warning: scale must be >= 1 and <= 4.\n");

	            scale = 1;
	        }

        } else if (!strcmp(argv[i], "--window")) {
	        windowed = 1;

        } else if (!strcmp(argv[i], "--dock")) {
	        docked = 1;

        } else if (!strcmp(argv[i], "--image")) {
	        image = argv[++i];

        } else if (!strcmp(argv[i], "-v") || !strcmp(argv[i], "--version")) {
            printf("%s version %s\n", NAME, VERSION);

            exit(1);

        } else {
            usage();

            exit(1);
        }
    }
}

void usage(void) {
    printf(
        "A simple PulseAudio volume control app.\n"
        "The left bar is to control the default sink, the right is for the default source. Single click on the volume number toggles mute.\n"
        "The app is based on wmamixer and pavolume.\n"
		"usage:\n"
		"     --display      DISPLAY   Use alternate X display.\n"
		"     --main-color   COLOR     Set main color.\n"
		"     --back-color   COLOR     Set background color.\n"
        "     --dark-color   COLOR     Set border dark color.\n"
        "     --light-color  COLOR     Set border light color.\n"
        "     --geometry     GEOMETRY  Set window geometry.\n"
        "     --scale        SCALE     Set dockapp scale (from 1 to 4).\n"
        "     --window                 Run in windowed mode.\n"
        "     --dock                   In the windowed mode, run as a panel.\n"
        "     --image        IMAGE     Set background xpm image.\n"
		"  -h --help                   Print this help.\n"
		"  -v --version                Show version info and exit.\n"
	);
}
