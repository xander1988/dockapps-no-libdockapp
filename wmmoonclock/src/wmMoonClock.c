/*
 *
 *  	wmMoonClock-1.26 (C) 1998, 1999 Mike Henderson (mghenderson@lanl.gov)
 *
 *  		- Shows Moon Phase....
 *
 *
 *
 *
 *
 * 	This program is free software; you can redistribute it and/or modify
 * 	it under the terms of the GNU General Public License as published by
 * 	the Free Software Foundation; either version 2, or (at your option)
 * 	any later version.
 *
 * 	This program is distributed in the hope that it will be useful,
 * 	but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * 	GNU General Public License for more details.
 *
 * 	You should have received a copy of the GNU General Public License
 * 	along with this program (see the file COPYING); if not, write to the
 * 	Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *      Boston, MA 02110-1301 USA
 *
 *      Things TODO:
 *                  - clean up code!
 *                  - more detailed documentation.
 *                  - reduce size of pixmap! Dont need it in one pixmap.
 *		      Aslo, does the hi-color pixmap really need all those colors?
 *                  - add rotation of moon so user sees it as they would in reality?
 *                  - eclipses. The calcs are quite acurate so this should be easily doable.
 *		      (Note the Sun position calcs in CalcEphem are low precision -- high is not
 * 		       as costly as the Moon calcs.) Sun posiiton is calculated but not used yet...
 *		    - Next new moons, next full moons, next quarters, etc...
 *		    - Moon names. I.e. Harvest, Blue, etc...
 *
 *
 *
 *      Changes:
 *      Version 1.27 - 	released June 7, 1999.
 *			fixed a minor bug in computation of azimuth (A in Horizon Coords). Thanks to
 *			Dan Moraru for spotting this one. (There were two SinGlat factors instead of one).
 *
 *      Version 1.26 - 	released April 22, 1999 (?).
 *      Version 1.25 - 	released March 22, 1999.
 *                     	Now auto-detects 8-bit display and forces the LowColor pixmap to
 *			be used. The -low option still works if you need to conserve colors
 *			even on high-color displays.
 *
 *			Added 3 command line options + code to change colors of the data
 *			entries:
 *
 *				-bc <Color> to change background color.
 *				-lc <Color> to change color of labels and headers.
 *				-dc <Color> to change color of data values.
 *
 *      Version 1.24 - 	released February 9, 1999.
 *                     	Added low color support via the -low command line option.
 *
 *      Version 1.23 - 	released February 4, 1999.
 *                     	cosmetic for AfterStep users. removed spurious black line at RHS edge an mask.
 *
 *	Version 1.22 - 	released January 8, 1999.
 *
 *		       	+ Changed PI2 to TwoPi in Moon.c -- Linux Pyth. had probs because
 *                        PI2 was defined in <math.h>.
 *
 *	Version 1.21 -  released January 7, 1999.
 *
 *                     + minor bug fixes in Makefile and manpage.
 *
 *	Version 1.2 - released January 3, 1999.
 *		      Added:
 *
 *			+ Local Time/ Universal Time display.
 *			+ Visible: Yes/No to indicate if Moon is up or not.
 *			+ Frac (percent through orbit -- this is NOT a simple
 *			  conversion of AGE....).
 *			+ Horizon Coords. Altitude is measured up from horizon to
 *			  Moon in degrees. Azimuth is in degrees from due south.
 *
 *       	      Also shuffled things around a bit...
 *
 *
 *
 *	Version 1.1 - released December 24, 1998.
 *                    Fixed bug in AGE calculation. It now should be highly accurate.
 *                    Note that AGE is not the same as Phase*29.530589 ...
 *                    I have checked with the Astronomical Almanac and it agrees very
 *		      well....
 *
 *
 *	Version 1.0 - released December 16, 1998.
 *
 *
 *
 */

/*
 *   Includes
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <time.h>
#include <X11/X.h>
#include <X11/Xlib.h>

#include "CalcEphem.h"
#include "MoonRise.h"
#include "dockapp.h"
#include "pixmaps/wmMoonClock_master.xpm"
#include "pixmaps/wmMoonClock_master_2.xpm"
#include "pixmaps/wmMoonClock_master_3.xpm"
#include "pixmaps/wmMoonClock_master_4.xpm"
#include "pixmaps/wmMoonClock_masterLow.xpm"
#include "pixmaps/wmMoonClock_masterLow_2.xpm"
#include "pixmaps/wmMoonClock_masterLow_3.xpm"
#include "pixmaps/wmMoonClock_masterLow_4.xpm"
#include "pixmaps/wmMoonClock_mask.xbm"
#include "pixmaps/wmMoonClock_mask_2.xbm"
#include "pixmaps/wmMoonClock_mask_3.xbm"
#include "pixmaps/wmMoonClock_mask_4.xbm"

/*
 *  Delay between refreshes (in microseconds)
 */
#define DELAY 1000000L

void ParseCMDLine(int argc, char *argv[]);
void pressEvent(XButtonEvent *xev);
void print_usage();

int	ToggleWindow = 0;
int	nMAX = 1;
int	Flag = 1;
int	UseLowColorPixmap = 0;

double Glat, Glon, SinGlat, CosGlat, TimeZone;

/*
 *   main
 */
int main(int argc, char *argv[]) {
    struct tm *GMTTime, *LocalTime;

    XEvent event;

    int i, n, j, ImageNumber, Year, Month, DayOfMonth, digit;
    int D, H, M, S, sgn, A, B, q;

    long CurrentLocalTime, CurrentGMTTime, date;

    double UT, val, RA, DEC, UTRise, UTSet, LocalHour, hour24();

    CTrans c;

    struct timeval timeout;

    fd_set xfdset;

    /*
     *  Parse any command line arguments.
     */
    Glat = Glon = 0.0;

    ParseCMDLine(argc, argv);

    c.Glat = Glat, c.Glon = Glon;

    Glat *= RadPerDeg; SinGlat = sin( Glat ); CosGlat = cos( Glat );

    init();

    if ((depth <= 8) || UseLowColorPixmap) {
        if (scale == 1)
            open_window(argc, argv, (char **)wmMoonClock_masterLow_xpm, (char *)wmMoonClock_mask_bits, wmMoonClock_mask_width, wmMoonClock_mask_height);
        if (scale == 2)
            open_window(argc, argv, (char **)wmMoonClock_masterLow_2_xpm, (char *)wmMoonClock_mask_2_bits, wmMoonClock_mask_2_width, wmMoonClock_mask_2_height);
        if (scale == 3)
            open_window(argc, argv, (char **)wmMoonClock_masterLow_3_xpm, (char *)wmMoonClock_mask_3_bits, wmMoonClock_mask_3_width, wmMoonClock_mask_3_height);
        if (scale == 4)
            open_window(argc, argv, (char **)wmMoonClock_masterLow_4_xpm, (char *)wmMoonClock_mask_4_bits, wmMoonClock_mask_4_width, wmMoonClock_mask_4_height);

    } else {
        if (scale == 1)
            open_window(argc, argv, (char **)wmMoonClock_master_xpm, (char *)wmMoonClock_mask_bits, wmMoonClock_mask_width, wmMoonClock_mask_height);
        if (scale == 2)
            open_window(argc, argv, (char **)wmMoonClock_master_2_xpm, (char *)wmMoonClock_mask_2_bits, wmMoonClock_mask_2_width, wmMoonClock_mask_2_height);
        if (scale == 3)
            open_window(argc, argv, (char **)wmMoonClock_master_3_xpm, (char *)wmMoonClock_mask_3_bits, wmMoonClock_mask_3_width, wmMoonClock_mask_3_height);
        if (scale == 4)
            open_window(argc, argv, (char **)wmMoonClock_master_4_xpm, (char *)wmMoonClock_mask_4_bits, wmMoonClock_mask_4_width, wmMoonClock_mask_4_height);
    }
    
    /*
     * Clear plotting area
     */
    copy_xpm_area(4, 69, 56, 56, 4, 4);

    /*
     *  Loop until we die
     */
    n = 32000;

    while (1) {
        if (Flag) {
            n = 32000;

            Flag = 0;
        }

        /*
         *  The Moon Ephemeris calculations are somewhat costly (the Moon is one of the most
         *  difficult objects to compute position for). So only process every nMAXth cycle of this
         *  loop. We run outer loop it faster to catch expose events, button presses, etc...
         *
         */
        if (n>nMAX) {
            n = 0;

            CurrentGMTTime = time(CurrentTime); GMTTime = gmtime(&CurrentGMTTime);

            UT = GMTTime->tm_hour + GMTTime->tm_min/60.0 + GMTTime->tm_sec/3600.0;

            Year = GMTTime->tm_year+1900;
            Month = GMTTime->tm_mon+1;
            DayOfMonth = GMTTime->tm_mday;

            date = Year*10000 + Month*100 + DayOfMonth;

            CurrentLocalTime = CurrentGMTTime; LocalTime = localtime(&CurrentLocalTime);

            LocalHour = LocalTime->tm_hour + LocalTime->tm_min/60.0 + LocalTime->tm_sec/3600.0;

            TimeZone = UT - LocalHour;

            CalcEphem(date, UT, &c);

            if (ToggleWindow == 0) {
                /*
                 *  Update Moon Image
                 */
                nMAX = 1000;

                ImageNumber = (int)(c.MoonPhase * 60.0 + 0.5);

                if (ImageNumber > 59)
                    ImageNumber = 0;

                if (Glat < 0)
                    ImageNumber = 59 - ImageNumber; /* add southern hemisphere support, closes: #537480 */

                j = ImageNumber/10;
                i = ImageNumber%10;

                copy_xpm_area(67+58*i, 2+58*j, 54, 54, 5, 5);

            } else if (ToggleWindow == 1) {
                /*
                 *  Update Numerical Display
                 */

                /* This requires second precision for LT and UT */
                nMAX = 0;

                /*
                 * Clear plotting area
                 */
                copy_xpm_area(4, 69, 56, 56, 4, 4);

                /*
                 *  Paste up LT and UT.
                 */
                val = LocalHour;

                H = (int)val; val = (val - H)*60.0;
                M = (int)val; val = (val - M)*60.0;
                S = (int)val;

                digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 25, 6);
                digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 25+5, 6);
                copy_xpm_area(117, 353, 1, 6, 25+10, 6);
                digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 25+12, 6);
                digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 25+17, 6);

                val = UT;
                H = (int)val; val = (val - H)*60.0;
                M = (int)val; val = (val - M)*60.0;
                S = (int)val;

                digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 25, 15);
                digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 25+5, 15);
                copy_xpm_area(117, 353, 1, 6, 25+10, 15);
                digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 25+12, 15);
                digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 25+17, 15);

                /*
                 *  Paste up AGE.
                 */
                val = c.MoonAge;

                q = (val < 10.0) ? 5 : 0;
                A = (int)val;

                val = (val - A)*100.0;

                B = (int)val;

                digit = A/10; if (digit != 0) copy_xpm_area(67+digit*5, 353, 5, 7, 26-q, 24);
                digit = A%10; copy_xpm_area(67+digit*5, 353, 5, 7, 26+5-q, 24);
                copy_xpm_area(62, 357, 3, 3, 26+11-q, 28);
                digit = B/10; copy_xpm_area(67+digit*5, 353, 5, 7, 26+15-q, 24);
                digit = B%10; copy_xpm_area(67+digit*5, 353, 5, 7, 26+20-q, 24);
                copy_xpm_area(143, 354, 3, 3, 26+25-q, 23);

                /*
                 *  Paste up Phase (Percent Illuminated).
                 */
                val = 0.5*( 1.0 - cos(c.MoonPhase*6.2831853) );
                val *= 100.0;
                A = (int)(val+0.5);
                if (A == 100){
                    copy_xpm_area(72, 353, 5, 7, 32+5, 42);
                    copy_xpm_area(67, 353, 5, 7, 32+10, 42);
                    copy_xpm_area(67, 353, 5, 7, 32+15, 42);
                        copy_xpm_area(147, 353, 5, 7, 32+20, 42);
                } else if (A >= 10){
                    digit = A/10; copy_xpm_area(67+digit*5, 353, 5, 7, 32+5, 42);
                    digit = A%10; copy_xpm_area(67+digit*5, 353, 5, 7, 32+10, 42);
                        copy_xpm_area(147, 353, 5, 7, 32+15, 42);
                } else {
                    digit = A; copy_xpm_area(67+digit*5, 353, 5, 7, 32+5, 42);
                        copy_xpm_area(147, 353, 5, 7, 32+10, 42);
                }

                /*
                 *  Paste up Frac (Percent of way through current lunar cycle).
                 */
                val = c.MoonPhase*100.0;
                A = (int)(val+0.5);
                if (A == 100){
                    copy_xpm_area(72, 353, 5, 7, 27+5, 33);
                    copy_xpm_area(67, 353, 5, 7, 27+10, 33);
                    copy_xpm_area(67, 353, 5, 7, 27+15, 33);
                        copy_xpm_area(147, 353, 5, 7, 27+20, 33);
                } else if (A >= 10){
                    digit = A/10; copy_xpm_area(67+digit*5, 353, 5, 7, 27+5, 33);
                    digit = A%10; copy_xpm_area(67+digit*5, 353, 5, 7, 27+10, 33);
                        copy_xpm_area(147, 353, 5, 7, 27+15, 33);
                } else {
                    digit = A; copy_xpm_area(67+digit*5, 353, 5, 7, 27+5, 33);
                        copy_xpm_area(147, 353, 5, 7, 27+10, 33);
                }

                /*
                 *  Paste up Visible Status.
                 */
                if (c.Visible)
                        copy_xpm_area(6,  327, 13, 6, 46, 51);
                else
                        copy_xpm_area(26, 327,  9, 6, 46, 51);

            } else if (ToggleWindow == 2){
                /*
                 *  Plot up Moon Rise/Set Times
                 */

                nMAX = 60;

                /*
                 * Clear plotting area
                 */
                    copy_xpm_area(4, 134, 56, 56, 4, 4);

                /*
                 *   Do Yesterday's first
                 */
                MoonRise(Year, Month, DayOfMonth-1, &UTRise, &UTSet);
                UTTohhmm(UTRise, &H, &M);

                if (H >= 0){
                    digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 7, 19);
                    digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+5, 19);
                    copy_xpm_area(117, 354, 1, 4, 7+10, 20);
                    digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+12, 19);
                    digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+17, 19);
                } else {
                    copy_xpm_area(57, 355, 5, 1, 7, 22); copy_xpm_area(57, 355, 5, 1, 7+5, 22);
                    copy_xpm_area(117, 354, 1, 4, 7+10, 20);
                    copy_xpm_area(57, 355, 5, 1, 7+12, 22); copy_xpm_area(57, 355, 5, 1, 7+17, 22);
                }

                UTTohhmm(UTSet, &H, &M);

                if (H >= 0){
                    digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 35, 19);
                    digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+5, 19);
                    copy_xpm_area(117, 354, 1, 4, 35+10, 20);
                    digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+12, 19);
                    digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+17, 19);
                } else {
                    copy_xpm_area(57, 355, 5, 1, 35, 22); copy_xpm_area(57, 355, 5, 1, 35+5, 22);
                    copy_xpm_area(117, 354, 1, 4, 35+10, 20);
                    copy_xpm_area(57, 355, 5, 1, 35+12, 22); copy_xpm_area(57, 355, 5, 1, 35+17, 22);
                }

                /*
                 *  Plot up todays Rise/Set times.
                 */
                MoonRise(Year, Month, DayOfMonth, &UTRise, &UTSet);
                UTTohhmm(UTRise, &H, &M);
                if (H >= 0){
                    digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 7, 29);
                    digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+5, 29);
                    copy_xpm_area(117, 354, 1, 4, 7+10, 30);
                    digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+12, 29);
                    digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+17, 29);
                } else {
                    copy_xpm_area(57, 355, 5, 1, 7, 32); copy_xpm_area(57, 355, 5, 1, 7+5, 32);
                    copy_xpm_area(117, 354, 1, 4, 7+10, 30);
                    copy_xpm_area(57, 355, 5, 1, 7+12, 32); copy_xpm_area(57, 355, 5, 1, 7+17, 32);
                }

                UTTohhmm(UTSet, &H, &M);

                if (H >= 0){
                    digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 35, 29);
                    digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+5, 29);
                    copy_xpm_area(117, 354, 1, 4, 35+10, 30);
                    digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+12, 29);
                    digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+17, 29);
                } else {
                    copy_xpm_area(57, 355, 5, 1, 35, 32); copy_xpm_area(57, 355, 5, 1, 35+5, 32);
                    copy_xpm_area(117, 354, 1, 4, 35+10, 30);
                    copy_xpm_area(57, 355, 5, 1, 35+12, 32); copy_xpm_area(57, 355, 5, 1, 35+17, 32);
                }

                /*
                 *  Plot up tomorrow's Rise/Set times.
                 */
                MoonRise(Year, Month, DayOfMonth+1, &UTRise, &UTSet);
                UTTohhmm(UTRise, &H, &M);
                if (H >= 0){
                    digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 7, 39);
                    digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+5, 39);
                    copy_xpm_area(117, 354, 1, 4, 7+10, 40);
                    digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+12, 39);
                    digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 7+17, 39);
                } else {
                    copy_xpm_area(57, 355, 5, 1, 7, 42); copy_xpm_area(57, 355, 5, 1, 7+5, 42);
                    copy_xpm_area(117, 354, 1, 4, 7+10, 40);
                    copy_xpm_area(57, 355, 5, 1, 7+12, 42); copy_xpm_area(57, 355, 5, 1, 7+17, 42);
                }

                UTTohhmm(UTSet, &H, &M);

                if (H >= 0){
                    digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 35, 39);
                    digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+5, 39);
                    copy_xpm_area(117, 354, 1, 4, 35+10, 40);
                    digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+12, 39);
                    digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 35+17, 39);
                } else {
                    copy_xpm_area(57, 355, 5, 1, 35, 42); copy_xpm_area(57, 355, 5, 1, 35+5, 42);
                    copy_xpm_area(117, 354, 1, 4, 35+10, 40);
                    copy_xpm_area(57, 355, 5, 1, 35+12, 42); copy_xpm_area(57, 355, 5, 1, 35+17, 42);
                }


            } else if (ToggleWindow == 3){
                /*
                 *  Plot up Horizon Coords
                 */
                nMAX = 3;

                /*
                 * Clear plotting area
                 */
                    copy_xpm_area(4, 199, 56, 56, 4, 4);

                /*
                 *  Paste up Azimuth, A
                 */
                val = c.A_moon;
                sgn = (val < 0.0) ? -1 : 0;
                val = fabs(val);
                D = (int)val;
                val = (val-(double)D)*100.0;
                M = (int)val;

                if (sgn < 0) copy_xpm_area(120, 357, 2, 1, 19, 27);

                /* degrees 100's */
                digit = D/100; copy_xpm_area(67+digit*5, 353, 5, 7, 22, 24);
                D -= digit*100;

                /* degrees 10's */
                digit = D/10; copy_xpm_area(67+digit*5, 353, 5, 7, 22+5, 24);

                /* degrees 1's */
                digit = D%10; copy_xpm_area(67+digit*5, 353, 5, 7, 22+10, 24);

                /* Decimal */
                    copy_xpm_area(62, 357, 3, 3, 22+15, 28);

                /*  Decimal Part */
                digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 22+19, 24);

                /*  mins 1's */
                digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 22+24, 24);

                copy_xpm_area(120, 353, 3, 3, 22+29, 23);

                /*
                 *  Paste up Altitude, h
                 */
                val = c.h_moon;
                sgn = (val < 0.0) ? -1 : 0;
                val = fabs(val);
                D = (int)val;
                val = (val-(double)D)*100.0;
                M = (int)val;

                if (sgn < 0) copy_xpm_area(120, 357, 2, 1, 19, 39);

                /* degrees 10's */
                digit = D/10; copy_xpm_area(67+digit*5, 353, 5, 7, 22, 36);

                /* degrees 1's */
                digit = D%10; copy_xpm_area(67+digit*5, 353, 5, 7, 22+5, 36);

                /* Decimal */
                    copy_xpm_area(62, 357, 3, 3, 22+10, 40);

                /*  Decimal Part */
                digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 22+14, 36);

                /*  mins 1's */
                digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 22+19, 36);

                copy_xpm_area(120, 353, 3, 3, 22+24, 35);

                /*
                 *  Paste up Earth-Moon Distance (in units of Earth radii).
                 */
                val = c.EarthMoonDistance;
                A = (int)val;
                val = (val - A)*100.0;
                B = (int)val;
                digit = A/10; if (digit != 0) copy_xpm_area(67+digit*5, 353, 5, 7, 30, 47);
                digit = A%10; copy_xpm_area(67+digit*5, 353, 5, 7, 30+5, 47);
                    copy_xpm_area(62, 357, 3, 3, 30+11, 51);
                digit = B/10; copy_xpm_area(67+digit*5, 353, 5, 7, 30+15, 47);
                digit = B%10; copy_xpm_area(67+digit*5, 353, 5, 7, 30+20, 47);

            } else if (ToggleWindow == 4){
                /*
                 *  Plot up RA/DEC Coords
                 */
                nMAX = 3;

                /*
                 * Clear plotting area
                 */
                    copy_xpm_area(4, 264, 56, 56, 4, 4);

                /*
                 *  Paste up Right Ascention
                 */
                RA = c.RA_moon/15.0;
                H = (int)RA;
                RA = (RA-(double)H)*60.0;
                M = (int)RA; RA = (RA-(double)M)*60.0;
                S = (int)(RA+0.5);

                /* hours 10's */
                digit = H/10; copy_xpm_area(67+digit*5, 353, 5, 7, 17, 25);

                /* hours 1's */
                digit = H%10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+5, 25);

                /* hour symbol */
                copy_xpm_area(138, 354, 3, 3, 17+10, 24);

                /*  mins 10's */
                digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+14, 25);

                /*  mins 1's */
                digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+19, 25);

                /* min symbol */
                copy_xpm_area(124, 353, 3, 3, 17+23, 24);

                /*  secs 10's */
                digit = S/10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+27, 25);

                /*  secs 1's */
                digit = S%10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+32, 25);

                /* sec symbol */
                copy_xpm_area(128, 353, 5, 3, 17+36, 24);

                /*
                 *  Paste up Declination
                 */
                DEC = c.DEC_moon;
                sgn = (DEC < 0.0) ? -1 : 0;
                DEC = fabs(DEC);
                D = (int)DEC;
                DEC = (DEC-(double)D)*60.0;
                M = (int)DEC;
                DEC = (DEC-(double)M)*60.0;
                S = (int)(DEC+0.5);

                if (sgn < 0) copy_xpm_area(120, 357, 2, 1, 14, 39);

                /* degrees 10's */
                digit = D/10; copy_xpm_area(67+digit*5, 353, 5, 7, 17, 36);

                /* degrees 1's */
                digit = D%10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+5, 36);

                /* degree symbol */
                copy_xpm_area(120, 353, 3, 3, 17+10, 35);

                /*  mins 10's */
                digit = M/10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+14, 36);

                /*  mins 1's */
                digit = M%10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+19, 36);

                /* min symbol */
                copy_xpm_area(124, 353, 3, 3, 17+23, 35);

                /*  secs 10's */
                digit = S/10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+27, 36);

                /*  secs 1's */
                digit = S%10; copy_xpm_area(67+digit*5, 353, 5, 7, 17+32, 36);

                /* sec symbol */
                copy_xpm_area(128, 353, 5, 3, 17+36, 35);

                /*
                 *  Paste up Earth-Moon Distance (in units of Earth radii).
                 */
                val = c.EarthMoonDistance;
                A = (int)val;
                val = (val - A)*100.0;
                B = (int)val;
                digit = A/10; if (digit != 0) copy_xpm_area(67+digit*5, 353, 5, 7, 30, 47);
                digit = A%10; copy_xpm_area(67+digit*5, 353, 5, 7, 30+5, 47);
                    copy_xpm_area(62, 357, 3, 3, 30+11, 51);
                digit = B/10; copy_xpm_area(67+digit*5, 353, 5, 7, 30+15, 47);
                digit = B%10; copy_xpm_area(67+digit*5, 353, 5, 7, 30+20, 47);
            }

        } else {
            /*
             *  Update the counter.
             */
            ++n;
        }

        /*
         *  Add X display to file descriptor set for polling.
         */
        FD_ZERO(&xfdset);
        FD_SET(ConnectionNumber(display), &xfdset);

        /*
         *   Process any pending X events.
         */
        while(XPending(display)){
            XNextEvent(display, &event);

            switch(event.type){
            case Expose:
                redraw_window();
                break;
            case ButtonPress:
                pressEvent(&event.xbutton);
                break;
            case ButtonRelease:
                break;
            case ClientMessage:
                if (event.xclient.data.l[0] == (int)deleteWin) {
                    exit(0);
                }
                break;
            }
        }

        /*
         *  Redraw and wait for next update
         */
        redraw_window();

        timeout.tv_sec = DELAY / 1000000L;
        timeout.tv_usec = DELAY % 1000000L;

        select(ConnectionNumber(display) + 1, &xfdset, NULL, NULL, &timeout);
    }
}

/*
 *   ParseCMDLine()
 */
void ParseCMDLine(int argc, char *argv[]) {
    int i;

    char *eptr;

    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "--display")) {
  	        display_name = argv[++i];

        } else if (!strcmp(argv[i], "--geometry")) {
	        geometry = argv[++i];

        } else if (!strcmp(argv[i], "--scale")) {
            char *tmp = argv[++i];

            if (tmp == NULL)
                scale = 1;
            else
	            scale = atoi(tmp);

	        if (scale < 1 || scale > 4) {
	            printf("Warning: scale must be >= 1 and <= 4.\n");

	            scale = 1;
	        }

        } else if (!strcmp(argv[i], "--window")) {
	        windowed = 1;

        } else if (!strcmp(argv[i], "--dock")) {
	        docked = 1;

        } else if (!strcmp(argv[i], "--image")) {
	        image = argv[++i];

        } else if (!strcmp(argv[i], "--dark-color")) {
            if ((i + 1 >= argc) || (argv[i + 1][0] == '-')) {
                fprintf(stderr, "wmMoonClock: --dark-color needs a color argument.\n");

                print_usage();

                exit(-1);
            }

	        dark_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--light-color")) {
            if ((i + 1 >= argc) || (argv[i + 1][0] == '-')) {
                fprintf(stderr, "wmMoonClock: --light-color needs a color argument.\n");

                print_usage();

                exit(-1);
            }

	        light_bd_color = argv[++i];

        } else if (!strcmp(argv[i], "--back-color")) {
            if ((i + 1 >= argc) || (argv[i + 1][0] == '-')) {
                fprintf(stderr, "wmMoonClock: --back-color needs a color argument.\n");

                print_usage();

                exit(-1);
            }

            back_color = argv[++i];

        } else if (!strcmp(argv[i], "--label-color")) {
            if ((i + 1 >= argc) || (argv[i + 1][0] == '-')) {
                fprintf(stderr, "wmMoonClock: --label-color needs a color argument.\n");

                print_usage();

                exit(-1);
            }

            label_color = argv[++i];

        } else if (!strcmp(argv[i], "--data-color")) {
            if ((i + 1 >= argc) || (argv[i + 1][0] == '-')) {
                fprintf(stderr, "wmMoonClock: --data-color needs a color argument.\n");

                print_usage();

                exit(-1);
            }

            data_color = argv[++i];

        } else if (!strcmp(argv[i], "--low")) {
	        UseLowColorPixmap = 1;

        } else if (!strcmp(argv[i], "-t") || !strcmp(argv[i], "--latitude")) {
            if (i + 1 >= argc) {
		        fprintf(stderr, "wmMoonClock: -t needs a value in degrees.\n");

		        print_usage();

		        exit(-1);

	        } else {
		        Glat = strtod(argv[i + 1], &eptr);

		        if ((Glat == 0.0) && (eptr == argv[i + 1])) {
                    fprintf(stderr, "wmMoonClock: could not convert latitude %s.\n", argv[i + 1]);

                    print_usage();

                    exit(-1);
		        }

		        ++i;
            }

        } else if (!strcmp(argv[i], "-n") || !strcmp(argv[i], "--longitude")) {
            if (i + 1 >= argc) {
		        fprintf(stderr, "wmMoonClock: -n needs a value in degrees.\n");

		        print_usage();

		        exit(-1);

	        } else {
		        Glon = strtod(argv[i + 1], &eptr);

		        if ((Glon == 0.0) && (eptr == argv[i + 1])) {
                    fprintf(stderr, "wmMoonClock: could not convert longitude %s.\n", argv[i + 1]);

                    print_usage();

                    exit(-1);
		        }

		        ++i;
            }

	    } else {
	        print_usage();

	        exit(1);
	    }
    }
}

void print_usage() {
    printf(
        "wmMoonClock version: %s\n"
		"usage:\n"
		"     --low                     Use lower color pixmap (for 8-bit displays).\n"
		"  -t --latitude     LATITUDE   Observers Latitude. Positive in northern\n"
		"                               hemisphere, negative in southern hemisphere.\n"
		"  -n --longitude    LONGITUDE  Observers Longitude.\n"
		"                               Greenwich is 0.0, and longitude\n"
		"                               increases positively toward the west.\n"
		"                               Alternatively, negative numbers can also\n"
		"                               be used to specify longitudes to the east of\n"
		"                               Greenwich).\n"
		"     --display      DISPLAY    Use alternate X display.\n"
		"     --back-color   COLOR      Set background color (e.g. #ffbb00 or orange).\n"
        "     --dark-color   COLOR      Set border dark color.\n"
        "     --light-color  COLOR      Set border light color.\n"
        "     --data-color   COLOR      Set data color.\n"
        "     --label-color  COLOR      Color for labels.\n"
        "     --geometry     GEOMETRY   Set window geometry.\n"
        "     --scale        SCALE      Set dockapp scale (from 1 to 4).\n"
        "     --window                  Run in windowed mode.\n"
        "     --dock                    In the windowed mode, run as a panel.\n"
        "     --imag         IMAGE      Set background xpm image.\n"
		"  -h --help                    Print this help.\n"
		"  -v --version                 Show version info and exit.\n",
		VERSION
	);
}

/*
 *  This routine handles button presses. Clicking in the window
 *  toggles the display.
 *
 */
void pressEvent(XButtonEvent *xev) {
    (void)xev;

    ++ToggleWindow;

    if (ToggleWindow > 4)
        ToggleWindow = 0;

    Flag = 1;

    return;
}
