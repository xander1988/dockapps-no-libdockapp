#ifndef MOONRISE_H
#define MOONRISE_H

void MoonRise(int, int, int, double*, double*);
void UTTohhmm(double, int*, int*);
void Interp(double, double, double, double*, double*, double*, double*, int*);

#endif
